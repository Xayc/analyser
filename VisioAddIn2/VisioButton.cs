﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VisioAddIn2
{
    class VisioButton
    {
        public string name;
        public string caption;
        public Delegate clickDelegate; 

        public VisioButton(string name, string caption, Delegate clickDelegate)
        {
            this.name = name;
            this.caption = caption;
            this.clickDelegate = clickDelegate;
        }
    }
}
