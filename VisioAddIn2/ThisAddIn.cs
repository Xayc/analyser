﻿using System;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using System.Xml.Linq;
using Office = Microsoft.Office.Core;
using Visio = Microsoft.Office.Interop.Visio;
using System.Diagnostics;
using System.Collections.Generic;
using app;

namespace VisioAddIn2 {

    public partial class ThisAddIn {
        private Office.CommandBars _cBars;
        private Office.CommandBar _cBar;
        private Office.CommandBarButton _button;
        Stopwatch watch = new Stopwatch();


        private void ThisAddIn_Startup(object sender, System.EventArgs e) {
            this.Application.DocumentOpened += new Microsoft.Office.Interop.Visio.EApplication_DocumentOpenedEventHandler(vsoApplication_DocumentOpened);
            this.Application.DocumentCreated += new Microsoft.Office.Interop.Visio.EApplication_DocumentCreatedEventHandler(vsoApplication_DocumentCreated);
            this.Application.DocumentChanged += new Microsoft.Office.Interop.Visio.EApplication_DocumentChangedEventHandler(vsoApplication_DocumentChanged);
            this.Application.Documents.Add("");
        }
        
        void vsoApplication_DocumentCreated(Microsoft.Office.Interop.Visio.Document doc) {
            OnDocumentCreatedOpenedChanged(doc);
        }

        void vsoApplication_DocumentOpened(Microsoft.Office.Interop.Visio.Document doc) {
            OnDocumentCreatedOpenedChanged(doc);
        }

        void vsoApplication_DocumentChanged(Microsoft.Office.Interop.Visio.Document doc) {
            OnDocumentCreatedOpenedChanged(doc);
        }

        private void OnDocumentCreatedOpenedChanged(Visio.Document doc)
        {
            _cBars = (Office.CommandBars)this.Application.CommandBars;
            _cBar = null;

            // Check to see whether the document that 
            // is in focus is based on our template.
            // This is our document, so load our toolbar fresh.
            try
            {
                _cBar = _cBars.Add(
                        "Analize",
                        Missing.Value,
                        Missing.Value,
                        true);

                _button = (Office.CommandBarButton)_cBar.Controls.Add(
                        Office.MsoControlType.msoControlButton,
                        Missing.Value,
                        Missing.Value,
                        Missing.Value,
                        true);

                _button.Caption = "Анализировать";
                _button.Style = Office.MsoButtonStyle.msoButtonCaption;
                _button.Tag = "Анализировать";

                // Transform the next three lines of code to a single line of code.
                _button.Click += new Office._CommandBarButtonEvents_ClickEventHandler(button_Click2);

                _cBar.Visible = true;

            }
            catch (Exception)
            {
                _cBar = _cBars["Analize"];
            }
            
        }

        private void button_Click2(Office.CommandBarButton Ctrl, ref bool CancelDefault)
        {
            Visio.Page targetPage = this.Application.ActivePage;

            List<Visio.Shape> shapes = new List<Visio.Shape>();
            foreach (var c in this.Application.ActiveWindow.Selection)
            {
                Visio.Shape shape = c as Visio.Shape;
                if (shape != null)
                    shapes.Add(shape);
            }
            VisioPage vp = new VisioPage(targetPage);
            vp.SetSelection(this.Application.ActiveWindow.Selection);
            vp.Analyze();
        }


        private void ThisAddIn_Shutdown(object sender, System.EventArgs e) {
        }

        #region VSTO generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup() {
            this.Startup += new System.EventHandler(ThisAddIn_Startup);
            this.Shutdown += new System.EventHandler(ThisAddIn_Shutdown);
        }

        #endregion
    }
}
