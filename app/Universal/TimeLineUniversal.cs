﻿using app.Diagramm;
using app.Vocabulary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace app.Universal
{
    public class TimeLineDiagramm
    {
        public List<Lines> TimeLines = new List<Lines>();

        public Microsoft.Msagl.Drawing.Graph MakeGraph()
        {
            var graph = new Microsoft.Msagl.Drawing.Graph();
            int i = 0;
            int step = 0;

            var last = new List<Microsoft.Msagl.Drawing.Node>();

            foreach (var node in TimeLines)
            {
                i++;
                step++;

                Microsoft.Msagl.Drawing.Node one = graph.AddNode(i.ToString());
                one.LabelText = step.ToString();
                one.Attr.Shape = Microsoft.Msagl.Drawing.Shape.Circle;

                foreach (var l in last)
                    graph.AddEdge(l.Id, one.Id);

                last.Clear();

                foreach (var v in node.Items)
                {
                    i++;
                    var gnode = graph.AddNode(i.ToString());
                    graph.AddEdge(one.Id, gnode.Id);
                    gnode.LabelText = v.Text;
                    last.Add(gnode);
                }
            }
            return graph;
        }
    }

    enum Position
    {
        Start,
        Middle, 
        End,
        None
    }

    public class Lines: IComparable<Lines>
    {
        public List<Vertex> Items = new List<Vertex>();
        public int Time;
        public List<Lines> Next = new List<Lines>();
        public List<Lines> Previous = new List<Lines>();

        public void AddNext(Lines other)
        {
            this.Next.Add(other);
            other.Previous.Add(this);
        }

        public void AddPrevious(Lines other)
        {
            other.AddNext(this);
        }

        public int CompareTo(Lines other)
        {
            return Time.CompareTo(other.Time);
        }

        public void RemoveSide(Lines other)
        {
            Next.Remove(other);
            Previous.Remove(other);
        }
    }

    class LinesGroup : List<Lines>
    {
        public void AddNext(Lines other)
        {
            foreach(var l in this)
            {
                l.AddNext(other);
            }
        }

        public void AddPrevious(Lines other)
        {
            foreach (var l in this)
            {
                l.AddPrevious(other);
            }
        }
    }

    public class TimeLineUniversal
    {
        Diagramm.Diagramm diagramm;
        TimeLineDiagramm timeline;

        public TimeLineUniversal(Diagramm.Diagramm d)
        {
            this.diagramm = d;
        }

        public void Clear()
        {
            foreach (var s in diagramm.Shapes())
            {
                s.Analyzed = false;
            }
        }

        bool IsVerb(Vertex v)
        {
            return Verb.IsVerb(v.Text);
        }

        void Collapse()
        {
            var list = new List<Vertex>(diagramm.Vertexes);
            foreach (var s in list)
            {
                if (s.Edges.Count == 0)
                    diagramm.CollapseVertex(s);
            }


            list = new List<Vertex>(diagramm.Vertexes);
            foreach (var s in list)
            {
                var collapsed = !IsVerb(s);
                if (collapsed)
                    diagramm.CollapseVertex(s);
            }
        }

        //void Collapse(LinesGroup groups)
        //{
        //    var linesList = new LinesGroup();
        //    linesList.AddRange(groups);

        //    foreach (var line in groups)
        //    {
        //        if (line.Next.Count != 1)
        //            continue;
        //        var next = line.Next.First();
        //        if (next.Previous.Count != 1)
        //            continue;
        //        next.Items.InsertRange(0, line.Items);
        //        foreach(var prev in line.Previous)
        //        {
        //            prev.RemoveSide(line);
        //            prev.AddNext(next);
        //        }
        //        linesList.Remove(line);
        //    }
        //    groups.Clear();
        //    groups.AddRange(linesList);
        //}

        //void old()
        //{
        //    var mapVetex = new Dictionary<Vertex, LinesGroup>();
        //    var linesList = new LinesGroup();

        //    foreach (var edge in diagramm.Edges)
        //    {
        //        if (edge.DirectionalType == EdgeDirectionalType.NoDirectional)
        //            continue;
        //        Lines lines = new Lines();
        //        linesList.Add(lines);
        //        lines.Items.Add(edge.BeginVertex);
        //        lines.Items.Add(edge.EndVertex);

        //        var i = 0;
        //        foreach (var v in lines.Items)
        //        {
        //            var groupsVertex = mapVetex.GetValueOrDefault(v, null);
        //            if (groupsVertex == null)
        //            {
        //                groupsVertex = new LinesGroup();
        //                mapVetex.Add(v, groupsVertex);
        //            }
        //            if (i == 0)
        //                groupsVertex.FindIncoming(v).AddNext(lines);
        //            else
        //                groupsVertex.FindOutgoing(v).AddPrevious(lines);
        //        }
        //    }

        //    Collapse(linesList);
        //}

        //class Point
        //{
        //    public Vertex Vertex;
        //    public Lines Line;
        //}

        public int minTimeVertex(Vertex v)
        {
            if (v.MineTime >= 0)
                return v.MineTime;

            foreach (var s in diagramm.Vertexes)
            {
                s.Analyzed = false;
            }

            Stack<Vertex> st = new Stack<Vertex>();
            st.Push(v);
            while (st.Count > 0)
            {
                var current = st.Pop();
                if (current.MineTime >= 0)
                    continue;

                current.MineTime = int.MaxValue;
                var egdes = current.Incoming();
                if (egdes.Count == 0)
                {
                    current.MineTime = v.Timer;
                }
                else if (egdes.All(e => e.BeginVertex.MineTime >= 0))
                {
                    current.MineTime = egdes.Min(e => e.BeginVertex.MineTime) + current.Timer;
                }
                else
                {
                    current.MineTime = -1;
                    if (st.Contains(current))
                    {
                        current.MineTime = 0;
                        continue;
                    }
                    st.Push(current);
                    foreach (var e in egdes)
                    {
                        st.Push(e.BeginVertex);
                    }
                }
            }
            return v.MineTime;
        }

        public void MinTime()
        {
            foreach (var v in diagramm.Vertexes)
            {
                v.MineTime = -1;
                v.Timer = 1;
            }

            foreach (var shape in diagramm.Vertexes)
            {
                Vertex v = shape as Vertex;
                if (v == null) continue;
                minTimeVertex(v);
            }
        }

        public TimeLineDiagramm CreateTimeLine()
        {
            this.timeline = new TimeLineDiagramm();
            Clear();
            Collapse();

            MinTime();


            var times = diagramm.Vertexes.Select(v => v.MineTime).ToList();
            var setTime = new HashSet<int>(times);
            var timesVertex = setTime.Select(t => new Lines()
            {
                Items = diagramm.Vertexes.FindAll(v => v.MineTime == t),
                Time = t
            });
            var list = timesVertex.ToList();
            list.Sort();
            this.timeline.TimeLines = list;

            return this.timeline;
        }
    }
}
