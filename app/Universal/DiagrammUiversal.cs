﻿using app.Diagramm;
using app.Petri;
using app.VisioBridge;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace app.Universal
{
    public class VertexTypeUniversal : ShapeFormBase
    {
        public string _name;

        public VertexTypeUniversal(string name)
        {
            this._name = name;
        }

        public override string Name
        {
            get
            {
                return _name;
            }
        }

        public override ShapeFormBase GetForm(string name)
        {
            return new VertexTypeUniversal(name);
        }
    }

    public class DiagrammTypeUniversal : DiagrammType
    {
        public override VisioDiagramm Visio()
        {
            return new VisioUniversal();
        }

        public override PetriNetsConvertor PetriNetsConvertor()
        {
            return null;
        }

        public override Analyzer GetAnalyzer()
        {
            return null;
        }
    }
}
