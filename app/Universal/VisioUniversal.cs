﻿using app.Diagramm;
using app.VisioBridge;
using System.Collections.Generic;
using System.Linq;
using Visio = Microsoft.Office.Interop.Visio;

namespace app.Universal
{
    class VisioUniversal : VisioDiagramm
    {
        static Dictionary<string, string> map = new Dictionary<string, string>
        {
            {"Kind Symbol", "Kind"},
            {"Process", "Process"},
            {"Process1", "Individual"},
            {"And Junction", "AND"},
            {"Or Junction", "OR"},
            {"Not Junction", "NOT"},
            {"Connect", "Connect"},
            {"Strong Transition Arrow", "StrongTransitionArrow"},
            {"SecondOrder", "SecondOrder"},
            {"Weak Transition", "WeakTransition"},
            {"Two-place Relation Symbol", "FirstOrder"},
        };

        public override Diagramm.Diagramm CreateDiagramm(List<Visio.Shape> Shapes)
        {
            var d = new Diagramm.Diagramm();
            foreach (Visio.Shape shape in Shapes)
            {
                string name = shape.NameU.Split('.')[0];

                // Обработка коннектора
                if (name == "Dynamic connector")
                {
                    Edge e = new Edge();
                    e.Id = shape.ID;
                    d.Add(e);

                    Visio.Cell cBegin = shape.CellsSRC[(short)Visio.VisSectionIndices.visSectionObject, (short)Visio.VisRowIndices.visRowLine, (short)Visio.VisCellIndices.visLineBeginArrow];
                    Visio.Cell cEnd = shape.CellsSRC[(short)Visio.VisSectionIndices.visSectionObject, (short)Visio.VisRowIndices.visRowLine, (short)Visio.VisCellIndices.visLineEndArrow];

                    bool arrowBegin = cBegin.get_ResultStr(Visio.VisUnitCodes.visNoCast) != "0";
                    bool arrowEnd = cEnd.get_ResultStr(Visio.VisUnitCodes.visNoCast) != "0";

                    foreach (Visio.Connect c in shape.Connects)
                    {
                        if (c.FromCell.Name == "BeginX")
                        {
                            e.BeginId = c.ToSheet.ID;
                        }
                        if (c.FromCell.Name == "EndX")
                        {
                            e.EndId = c.ToSheet.ID;
                        }
                    }

                    // Определение направления
                    e.DirectionalType = EdgeDirectionalType.Directional;
                    if (!arrowBegin && !arrowEnd)
                    {
                        e.DirectionalType = EdgeDirectionalType.NoDirectional;
                    }
                    if (arrowBegin && arrowEnd)
                    {
                        throw new DiarammException(shape.ID, "Двухсторонняя связь");
                    }

                    if (arrowBegin)
                    {
                        int t = e.BeginId;
                        e.BeginId = e.EndId;
                        e.EndId = t;
                    }

                    continue;
                }

                Vertex v = new Vertex();
                v.Text = shape.Text;
                v.Id = shape.ID;
                v.Type = new VertexTypeUniversal(name);
                d.Add(v);
            }


            d.RemoveHanginRelation();
            d.Close();
            return d;
        }
    }

}
