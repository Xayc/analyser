﻿using app.BPMN;
using app.Diagramm;
using app.EPC;
using app.Idef3;
using app.Idef5;
using app.Kompas;
using app.Universal;
using app.VisioBridge;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Visio = Microsoft.Office.Interop.Visio;

namespace app
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Analyze(string file)
        {
            //try
            //{
            //    Diagramm d = Diagramm.Load($"{file}.json");

            //    Analyzer a = new Analyzer();
            //    a.Diagramm = d;
            //    a.Run();

            //    var nets = a.PetriNets();
            //    nets.CalculateXY();
            //    nets.Save($"{file}-petri.xml");
            //    ShowNets(nets);
            //}
            //catch (DiarammException e)
            //{
            //    MessageBox.Show(e.Message, "Ошибка");
            //}
        }

        private void ShowNets(Microsoft.Msagl.Drawing.Graph graph)
        {
            var form = new System.Windows.Forms.Form();
            var viewer = new Microsoft.Msagl.GraphViewerGdi.GViewer();
            viewer.Graph = graph;
            form.SuspendLayout();
            viewer.Dock = System.Windows.Forms.DockStyle.Fill;
            form.Controls.Add(viewer);
            form.ResumeLayout();
            form.ShowDialog();
        }

        private void Button_Click(object sender, RoutedEventArgs args)
        {
            //Analyze("Правильно-просто");
            //return;

            VisioPage vp = GetVisioPage();
            vp.Analyze();

            //VisioPage vp = new VisioPage(targetPage);
            //vp.SetSelection(app.ActiveWindow.Selection);
            //var nets = vp.Analyze();
            //if (nets != null)
            //    ShowNets(nets);
        }

        DiagrammType SelectedDiagrammType()
        {
            string type = (CbTypeDiagramm.SelectedValue as TextBlock).Text;
            if (type == "IDEF3")
                return new DiagrammTypeIdef3();
            if (type == "EPC")
                return new DiagrammTypeEPC();
            if (type == "BPMN")
                return new DiagrammTypeBPMN();
            if (type == "IDEF5")
                return new DiagrammTypeIdef5();
            return null;
        }

        VisioPage GetVisioPage()
        {
            var vp = VisioPage.Create();
            vp.DiagrammType = SelectedDiagrammType();
            return vp;
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            VisioPage vp = GetVisioPage();
            var nets = vp.CreatePetri();
            if (nets != null)
                ShowNets(nets.MakeGraph());
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            VisioPage vp = GetVisioPage();
            vp.DiagrammType = new DiagrammTypeUniversal();
            var nets = vp.TimeLineDiagramm();
            if (nets != null)
                ShowNets(nets.MakeGraph());
        }

        private void kompas_Click(object sender, RoutedEventArgs e)
        {
            var app = (Visio.Application)Marshal.GetActiveObject("Visio.Application");
            var generator = new VisioGenerator(app.Documents, app.ActivePage);
            generator.Run();
        }

        private void StepByStep_Click(object sender, RoutedEventArgs e)
        {
            var diagramType = SelectedDiagrammType();
            if (diagramType is DiagrammTypeEPC)
            {
                var win = new Debug();
                win.ShowDialog();
            }
        }
    }
}
