﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Visio = Microsoft.Office.Interop.Visio;

namespace app.Kompas
{

    public class VisioGenerator
    {
        Visio.Documents visioDocs;
        Visio.Page visioPage;
        List<Node> Nodes = new List<Node>();

        Visio.Master eventMaster;
        Visio.Master funcitonMaster;
        Visio.Master connectorMaster;

        public VisioGenerator(Visio.Documents visioDocs, Visio.Page visioPage)
        {
            this.visioDocs = visioDocs;
            this.visioPage = visioPage;
        }

        void LoadXML()
        {
            Nodes.Clear();
            XmlDocument xDoc = new XmlDocument();
            xDoc.Load("operaion.xml");
            XmlElement xRoot = xDoc.DocumentElement;
            foreach (XmlNode xnode in xRoot)
            {
                var node = new Node();
                node.Name = xnode.Attributes.GetNamedItem("name").InnerText;
                node.Type = xnode.Attributes.GetNamedItem("type").InnerText;
                node.TopType = xnode.Attributes.GetNamedItem("toptype").InnerText;
                Nodes.Add(node);
            }
        }

        void Connect(Visio.Shape shape1, Visio.Shape shape2)
        {
            Visio.Shape connector = visioPage.Drop(connectorMaster, 4.50, 4.50);
            connector.get_Cells("EndArrow").Formula = "=5";

            Visio.Cell beginXCell = connector.get_CellsSRC(
            (short)Visio.VisSectionIndices.visSectionObject,
            (short)Visio.VisRowIndices.visRowXForm1D,
            (short)Visio.VisCellIndices.vis1DBeginX);

            beginXCell.GlueTo(shape1.get_CellsSRC(
            (short)Visio.VisSectionIndices.visSectionObject,
            (short)Visio.VisRowIndices.visRowXFormOut,
            (short)Visio.VisCellIndices.visXFormPinX));

            Visio.Cell endXCell = connector.get_CellsSRC(
            (short)Visio.VisSectionIndices.visSectionObject,
            (short)Visio.VisRowIndices.visRowXForm1D,
            (short)Visio.VisCellIndices.vis1DEndX);


            endXCell.GlueTo(shape2.get_CellsSRC(
            (short)Visio.VisSectionIndices.visSectionObject,
            (short)Visio.VisRowIndices.visRowXFormOut,
            (short)Visio.VisCellIndices.visXFormPinX));
        }

        void Draw()
        {
            List<Visio.Shape> shapes = new List<Visio.Shape>();
            double x = 1;
            double y = 11;

            Visio.Shape eventShapeStart = visioPage.Drop(eventMaster, x, y);
            eventShapeStart.Text = "Получено задание";
            x += 1;
            shapes.Add(eventShapeStart);

            foreach (var node in Nodes)
            {
                Visio.Shape funcShape = visioPage.Drop(funcitonMaster, x, y);
                funcShape.Text = "Построить " + node.Type;
                shapes.Add(funcShape);
                x += 1;

                Visio.Shape eventShape = visioPage.Drop(eventMaster, x, y);
                eventShape.Text = "Выполнено";
                shapes.Add(eventShape);
                x += 1;

                if (x > 7)
                {
                    x = 1;
                    y -= 1;
                }
                
            }

            Visio.Shape last = null;
            foreach(var shape in shapes)
            {
                if (last != null)
                    Connect(last, shape);
                last = shape;
            }
        }

        public void Run()
        {
            string docPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyDocuments) + @"\Мои фигуры\epc.vssx";
            docPath = "EPC_M.vssx";
            Visio.Document visioStencil = visioDocs.OpenEx(docPath, (short)Visio.VisOpenSaveArgs.visOpenDocked);
            eventMaster = visioStencil.Masters.get_ItemU("Event");
            funcitonMaster = visioStencil.Masters.get_ItemU("Function");

            Visio.Document currentStencil = visioDocs.OpenEx("Basic Flowchart Shapes (US units).vss", (short)Visio.VisOpenSaveArgs.visOpenDocked);
            connectorMaster = currentStencil.Masters.get_ItemU("Dynamic connector");
            LoadXML();
            Draw();
        }
    }

    public class Node
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public string TopType { get; set; }
    }
}
