﻿using app.Idef3;
using app.VisioBridge;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;

namespace app.Diagramm
{
    /// <summary>
    ///  Ошибка разбора диаграммы
    /// </summary>
    public class DiarammException : Exception
    {
        public int ShapeId;
        public ShapeGeneric Shape;
        public DiarammException(int ShapeId, string message, ShapeGeneric shape = null) : base(message)
        {
            this.ShapeId = ShapeId;
            this.Shape = shape;
        }
    }

    /// <summary>
    ///  Диаграмма
    /// </summary>
    [DataContract]
    public class Diagramm
    {
        [DataMember]
        public List<Vertex> Vertexes = new List<Vertex>();
        [DataMember]
        public List<Edge> Edges = new List<Edge>();

        public Dictionary<int, Vertex> IdToVertex = new Dictionary<int, Vertex>();
        public Dictionary<int, Edge> IdToEdge = new Dictionary<int, Edge>();
        public Dictionary<int, ShapeGeneric> IdToShape = new Dictionary<int, ShapeGeneric>();

        /// <summary>
        ///  Добавление узла
        /// </summary>
        public void Add(Vertex v)
        {
            Vertexes.Add(v);
            IdToVertex[v.Id] = v;
            IdToShape[v.Id] = v;
        }

        /// <summary>
        ///  Добавление дуги
        /// </summary>
        public void Add(Edge e)
        {
            Edges.Add(e);
            IdToEdge[e.Id] = e;
            IdToShape[e.Id] = e;
        }

        /// <summary>
        ///  Построение словарей для поиска дуг и узлов
        /// </summary>
        public void CreateMap()
        {
            if (IdToVertex == null)
                IdToVertex = new Dictionary<int, Vertex>();
            if (IdToEdge == null)
                IdToEdge = new Dictionary<int, Edge>();

            foreach (var v in Vertexes)
            {
                IdToVertex[v.Id] = v;

                if (v.Edges == null)
                    v.Edges = new List<Edge>();
            }
            foreach (var e in Edges)
            {
                IdToEdge[e.Id] = e;
            }
        }

        public void RemoveHanginRelation()
        {
            Edges.RemoveAll(e => !IdToShape.ContainsKey(e.BeginId) || !IdToShape.ContainsKey(e.EndId));
        }

        /// <summary>
        ///  Закрытие диаграммы для имзенения
        /// </summary>
        public void Close()
        {
            //Save("tmp.json");

            foreach (var v in Vertexes)
            {
                v.Edges.Clear();
            }
            foreach (var e in Edges)
            {
                try
                {
                    e.Begin = IdToShape[e.BeginId];
                    e.End = IdToShape[e.EndId];
                }
                catch (KeyNotFoundException)
                {
                    throw new DiarammException(e.Id, "Неполня связь");
                }
                e.Begin.Edges.Add(e);
                e.End.Edges.Add(e);               
            }
        }

        /// <summary>
        ///  Сохранение диагрммы в файл
        /// </summary>
        public void Save(string file)
        {
            DataContractJsonSerializer jsonFormatter = new DataContractJsonSerializer(typeof(Diagramm));
            var stream = new MemoryStream();
            jsonFormatter.WriteObject(stream, this);
            stream.Position = 0;
            using (var reader = new StreamReader(stream))
            {
                string data = reader.ReadToEnd();
                dynamic parsedJson = JsonConvert.DeserializeObject(data);
                data = JsonConvert.SerializeObject(parsedJson, Formatting.Indented);
                System.IO.File.WriteAllText(file, data);
            }
        }

        /// <summary>
        ///  Заугрзка диагрммы из файла
        /// </summary>
        public static Diagramm Load(string file)
        {
            DataContractJsonSerializer jsonFormatter = new DataContractJsonSerializer(typeof(Diagramm));
            using (var reader = new FileStream(file, FileMode.Open))
            {
                var d = jsonFormatter.ReadObject(reader) as Diagramm;
                d.CreateMap();
                d.Close();
                return d;
            }
        }

        /// <summary>
        ///  Список элементов в даграмме
        /// </summary>
        public List<ShapeGeneric> Shapes()
        {
            List<ShapeGeneric> items = new List<ShapeGeneric>(Vertexes);
            items.AddRange(Edges);
            return items;
        }

        public void CollapseVertex(Vertex v)
        {
            this.Vertexes.Remove(v);
            Edges.RemoveAll(e => e.BeginVertex == v || e.EndVertex == v);
            
            foreach(var income in v.Incoming())
            {
                var berfore = income.BeginVertex;
                foreach(var outgoing in v.Outgoing())
                {
                    var next = outgoing.EndVertex;
                    Edge e = new Edge();
                    e.BeginId = berfore.Id;
                    e.EndId = next.Id;
                    e.DirectionalType = EdgeDirectionalType.Directional;
                    Add(e);
                }
            }
            Close();
        }
    }

    /// <summary>
    ///  Описание диаграммы определённого типа
    /// </summary>
    public abstract class DiagrammType
    {
        public abstract VisioDiagramm Visio();
        public abstract Petri.PetriNetsConvertor PetriNetsConvertor();
        public abstract Analyzer GetAnalyzer();
    }
}
