﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace app.Diagramm
{
    /// <summary>
    ///  Абстратный класс для анализа диграмм
    /// </summary>
    public abstract class Analyzer
    {
        public Diagramm Diagramm;
        public abstract void Run();
        public abstract int MinTime();
    }
}
