﻿using app.Idef3;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;

namespace app.Diagramm
{
    /// <summary>
    ///  Типы элементов в диаграмме
    /// </summary>
    public enum ShapeType { Vertex, Edge };

    /// <summary>
    ///  Элемент диаграммы
    /// </summary>
    [DataContract]
    public abstract class ShapeGeneric
    {
        [DataMember]
        public int Id;

        public bool Analyzed;

        /// <summary>
        ///  Типы элемента
        /// </summary>
        public abstract ShapeType ShapeType
        {
            get;
        }

        /// <summary>
        ///  Узел
        /// </summary>
        public Vertex Vertex()
        {
            return this as Vertex;
        }
        /// <summary>
        ///  Дуга
        /// </summary>
        public Edge Edge()
        {
            return this as Edge;
        }

        /// <summary>
        ///  Соседние элементы
        /// </summary>
        public List<ShapeGeneric> Neighbors()
        {
            if (ShapeType == ShapeType.Edge)
                return new List<ShapeGeneric>(new ShapeGeneric []{ Edge().Begin, Edge().End });
            return new List<ShapeGeneric>(Vertex().Edges);
        }

        public List<Edge> Edges = new List<Edge>();
    }

    /// <summary>
    ///  Общий класс для элементов диаграмм
    /// </summary>
    public abstract class ShapeFormBase
    {
        /// <summary>
        ///  Имя элемента
        /// </summary>
        public abstract string Name
        {
            get;
        }

        /// <summary>
        ///  Формрование элемента по имени
        /// </summary>
        public abstract ShapeFormBase GetForm(string name);
    }

    /// <summary>
    ///  Направленность дуг
    /// </summary>
    public enum EdgeDirectionalType
    {
        Directional,
        NoDirectional
    };

    /// <summary>
    ///  Менеджер для работы с направлением дуг
    /// </summary>
    public static class EdgeDirectionalTypeManager
    {
        static Dictionary<object, string> map = new Dictionary<object, string>
        {
            {EdgeDirectionalType.Directional, "Directional"},
            {EdgeDirectionalType.NoDirectional, "NoDirectional"}
        };

        /// <summary>
        ///  Получение направления дуги по имени
        /// </summary>
        public static EdgeDirectionalType GetEdgeType(string name)
        {
            return (EdgeDirectionalType)map.First(t => t.Value == name).Key;
        }

        /// <summary>
        ///  Получение кода направления
        /// </summary>
        public static string Name(EdgeDirectionalType type)
        {
            return map[type];
        }
    }

    /// <summary>
    ///  Узел диагрммы
    /// </summary>
    [DataContract]
    public class Vertex : ShapeGeneric
    {
        [DataMember]
        public string Text;
        public ShapeFormBase Type;
        public Dictionary<string, string> Data = new Dictionary<string, string>();

        [DataMember]
        public int Timer;

        public int MineTime = 0;

        /// <summary>
        ///  Типы узла
        /// </summary>
        [DataMember]
        public string TypeName
        {
            get
            {
                return Type.Name;
            }
            set
            {
                Type = Type.GetForm(value);
            }
        }

        /// <summary>
        ///  Список дуг определённой направленности
        /// </summary>
        public List<Edge> EdgesByTypes(EdgeDirectionalType t)
        {
            return Edges.FindAll(e => e.DirectionalType == t);
        }

        /// <summary>
        ///  Входящие дуги
        /// </summary>
        public List<Edge> Incoming()
        {
            return EdgesByTypes(EdgeDirectionalType.Directional).FindAll(e => e.EndVertex == this);
        }

        /// <summary>
        ///  Исходящие дуги
        /// </summary>
        public List<Edge> Outgoing()
        {
            return EdgesByTypes(EdgeDirectionalType.Directional).FindAll(e => e.BeginVertex == this);
        }

        /// <summary>
        ///  Тип элемента
        /// </summary>
        public override ShapeType ShapeType
        {
            get { return ShapeType.Vertex; }
        }

    }

    /// <summary>
    ///  Дуга диаграммы
    /// </summary>
    [DataContract]
    public class Edge : ShapeGeneric
    {
        public ShapeGeneric Begin;
        public ShapeGeneric End;

        /// <summary>
        ///  Начальный узел
        /// </summary>
        public Vertex BeginVertex
        {
            get
            {
                return Begin as Vertex;
            }
        }

        /// <summary>
        ///  Конечный узел
        /// </summary>
        public Vertex EndVertex
        {
            get
            {
                return End as Vertex;
            }
        }

        public ShapeFormBase Type;
        public EdgeDirectionalType DirectionalType;

        [DataMember]
        public int BeginId;
        [DataMember]
        public int EndId;

        /// <summary>
        ///  Направление дуги
        /// </summary>
        [DataMember]
        public string DirectionalTypeName
        {
            get
            {
                return EdgeDirectionalTypeManager.Name(DirectionalType);
            }
            set
            {
                DirectionalType = EdgeDirectionalTypeManager.GetEdgeType(value);
            }
        }

        /// <summary>
        ///  Тип дуги
        /// </summary>
        [DataMember]
        public string TypeName
        {
            get
            {
                if (Type == null)
                    return null;
                return Type.Name;
            }
            set
            {
                DirectionalType = EdgeDirectionalTypeManager.GetEdgeType(value);
            }
        }

        /// <summary>
        ///  Тип элемента
        /// </summary>
        public override ShapeType ShapeType
        {
            get { return ShapeType.Edge; }
        }

        /// <summary>
        ///  Список соседних узлов
        /// </summary>
        public List<Vertex> Vertexes()
        {
            return new List<Vertex> { BeginVertex, EndVertex };
        }

        /// <summary>
        ///  Входящие дуги
        /// </summary>
        public List<Edge> Incoming()
        {
            return Edges.FindAll(e => e.End == this);
        }

        /// <summary>
        ///  Исходящие дуги
        /// </summary>
        public List<Edge> Outgoing()
        {
            return Edges.FindAll(e => e.Begin == this);
        }
    }
}
