﻿using app.Diagramm;
using app.Petri;
using app.VisioBridge;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace app.EPC
{
    public enum VertexTypesEPC
    {
        Event,
        Function,
        Process,
        Information,
        Unit,
        AND,
        OR,
        XOR
    };

    public class VertexTypeEPC : ShapeFormBase
    {
        public static ShapeFormBase CreateFromType(string name)
        {
            var typeidef = new VertexTypeEPC();
            typeidef.VertexType = map.First(t => t.Value == name).Key;
            return typeidef;
        }

        static Dictionary<VertexTypesEPC, string> map = new Dictionary<VertexTypesEPC, string>
        {
            {VertexTypesEPC.Event, "Event"},
            {VertexTypesEPC.Function, "Function"},
            {VertexTypesEPC.Process, "Process"},
            {VertexTypesEPC.Information, "Information"},
            {VertexTypesEPC.Unit, "Unit"},
            {VertexTypesEPC.AND, "AND"},
            {VertexTypesEPC.OR, "OR"},
            {VertexTypesEPC.XOR, "XOR"},
        };

        public VertexTypesEPC VertexType;
        public override ShapeFormBase GetForm(string name)
        {
            return CreateFromType(name);
        }

        public override string Name
        {
            get
            {
                return map[VertexType];
            }
        }
    }

    public class DiagrammTypeEPC : DiagrammType
    {
        public override VisioDiagramm Visio()
        {
            return new VisioEPC();
        }

        public override PetriNetsConvertor PetriNetsConvertor()
        {
            return new PetriNetsConvertorEPC();
        }

        public override Analyzer GetAnalyzer()
        {
            return new AnalyzerEPC();
        }
    }
}
