﻿using app.Diagramm;
using System;
using Excel = Microsoft.Office.Interop.Excel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.IO;

namespace app.EPC
{
    public class StatEpc
    {
        List<SaveItem> data = new List<SaveItem>();
        string fileDataName = "epc/data.csv";
        bool withTime = true;

        class SaveItem
        {
            public int vertexes;
            public int edges;
            public int edgesFork;
            public double ratio;
            public long time;
            public long steps;

            public double forkRatio()
            {
                return (double)edgesFork / edges;
            }

            public int size()
            {
                return vertexes + edges;
            }
        }

        class ChartDescription
        {
            public Excel.Worksheet xlWorkSheet;
            public Excel.Worksheet xlWorkSheet2;
            public List<SaveItem> items;
            public string title;
            public int startIndex;
            public int number;
            public string xTitle;
            public string yTitle;
            public string xColumn;
            public string yColumn;
        }


        public int edgesFork(Diagramm.Diagramm  d)
        {
            List<VertexTypesEPC> conditions = new List<VertexTypesEPC> { VertexTypesEPC.OR, VertexTypesEPC.XOR, VertexTypesEPC.AND};
            int result = 0;
            foreach (var vertex in d.Vertexes)
            {
                var vertexType = (vertex.Type as VertexTypeEPC).VertexType;
                if (conditions.IndexOf(vertexType) == -1)
                    continue;
                result += vertex.Outgoing().Count;
            }
            return result;
        }

        SaveItem saveDiagramm(Diagramm.Diagramm d)
        {
            string name = $"epc/src/{d.Vertexes.Count}-{d.Edges.Count}.json";
            //d.Save(name);

            var item = new SaveItem();
            item.vertexes = d.Vertexes.Count;
            item.edges = d.Edges.Count;
            item.ratio = item.vertexes / item.edges;
            item.edgesFork = edgesFork(d);
            data.Add(item);

            Stopwatch sw = new Stopwatch();
            Analyzer a = new DiagrammTypeEPC().GetAnalyzer();
            a.Diagramm = d;
            sw.Start();
            a.Run();
            if (withTime)
                a.MinTime();
            sw.Stop();
            item.time = sw.ElapsedMilliseconds;
            item.steps = (a as AnalyzerEPC).Steps();
            return item;
        }

        void CreateDiaramms()
        {
            int points = 100;
            points = 100;
            int maxVertex = 100000;
            maxVertex = 100000;
            int delta = maxVertex / points;
            int levelNumber = 5;
            int cases = 1;
            double levelDelta = ((double)1.0) / levelNumber;

            using (StreamWriter outputFile = new StreamWriter(fileDataName))
            {
                for (var c = 0; c < cases; c++)
                {
                    for (var n = 0; n <= levelNumber; n++)
                    {
                        for (var i = 1; i <= points; i++)
                        {
                            int number = n * points + i;
                            double fork = n * levelDelta;
                            Console.WriteLine($"{number}/{points * levelNumber} (fork-{fork}) ({delta * i}) ({c})");
                            GeneratorEPC generator = new GeneratorEPC(delta * i, fork);
                            var d = generator.Generate();
                            var item = saveDiagramm(d);
                            saveCsvItem(outputFile, item);
                            outputFile.Flush();
                        }
                    }
                }
            }

        }

        public void CreateData()
        {
            data = new List<SaveItem>();
            CreateDiaramms();            
            saveData();
        }

        void LoadData()
        {
            data.Clear();
            using (var reader = new StreamReader(fileDataName))
            {
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split('\t');

                    var item = new SaveItem();
                    item.vertexes = int.Parse(values[0]);
                    item.edges = int.Parse(values[1]);
                    item.ratio = double.Parse(values[2]);
                    item.time = int.Parse(values[3]);
                    item.edgesFork = int.Parse(values[4]);
                    item.steps = int.Parse(values[6]);
                    data.Add(item);
                    //if (data.Count > 1000)
                    //    return;
                }
            }
        }

        public void CreateCharts()
        {
            LoadData();
            saveChart();
        }

        void saveCsvItem(StreamWriter outputFile, SaveItem item)
        {
            outputFile.WriteLine($"{item.vertexes}\t{item.edges}\t{item.ratio}\t{item.time}\t{item.edgesFork}\t{(double)item.edgesFork / item.edges}\t{item.steps}");
        }

        void saveData()
        {
            using (StreamWriter outputFile = new StreamWriter(fileDataName))
            {
                foreach (var item in data)
                {
                    saveCsvItem(outputFile, item);
                }
            }
        }

        void DataToExcel(Excel.Worksheet xlWorkSheet, List<SaveItem> items, int startIndex)
        {
            var index = startIndex;
            foreach (var item in items)
            {
                xlWorkSheet.Cells[index, 1] = item.vertexes;
                xlWorkSheet.Cells[index, 2] = item.edges;
                xlWorkSheet.Cells[index, 3] = item.size();
                xlWorkSheet.Cells[index, 4] = item.ratio;
                xlWorkSheet.Cells[index, 5] = item.time;
                xlWorkSheet.Cells[index, 6] = item.steps;
                xlWorkSheet.Cells[index, 7] = item.edgesFork;
                xlWorkSheet.Cells[index, 8] = item.forkRatio();
                index++;
            }
        }

        void CreateChartDiagramm(ChartDescription chart)
        {
            object misValue = System.Reflection.Missing.Value;
            var index = chart.startIndex + chart.items.Count;

            Excel.Range chartRange;

            Excel.ChartObjects xlCharts = (Excel.ChartObjects)chart.xlWorkSheet2.ChartObjects(Type.Missing);

            var height = 250;

            Excel.ChartObject myChart = (Excel.ChartObject)xlCharts.Add(10, (height + 100) * chart.number + 100, 600, height);
            Excel.Chart chartPage = myChart.Chart;

            chartPage.HasTitle = true;
            chartPage.ChartTitle.Text = chart.title;

            chartRange = chart.xlWorkSheet.get_Range($"{chart.xColumn}{index}", $"{chart.xColumn}{index - 1}");
            chartPage.SetSourceData(chartRange, misValue);
            chartPage.ChartType = Excel.XlChartType.xlXYScatter;
            //chartPage.ChartType = Excel.XlChartType.xlLineMarkers;

            chartPage.HasLegend = false;
            var yAxis = (Excel.Axis)chartPage.Axes(Excel.XlAxisType.xlValue, Excel.XlAxisGroup.xlPrimary);
            yAxis.HasTitle = true;
            yAxis.AxisTitle.Text = chart.yTitle;

            var xAxis = (Excel.Axis)chartPage.Axes(Excel.XlAxisType.xlCategory, Excel.XlAxisGroup.xlPrimary);
            xAxis.HasTitle = true;
            xAxis.AxisTitle.Text = chart.xTitle;

            Excel.Series series = (Excel.Series)chartPage.SeriesCollection(1);
            series.Name = "Name ser 1";
            series.XValues = chart.xlWorkSheet.get_Range($"{chart.xColumn}{chart.startIndex}", $"{chart.xColumn}{index - 1}");
            series.Values = chart.xlWorkSheet.get_Range($"{chart.yColumn}{chart.startIndex}", $"{chart.yColumn}{index - 1}");
        }

        void CreateChartDiagrammAll(Excel.Worksheet xlWorkSheet, List<SaveItem> items, string fork, int startIndex, int number, Excel.Worksheet sheetTimeVertex, Excel.Worksheet sheetStepsVertex, Excel.Worksheet sheetTimeElems, Excel.Worksheet sheetStepsElems)
        {
            object misValue = System.Reflection.Missing.Value;

            ChartDescription chartStepsVertex = new ChartDescription();
            chartStepsVertex.title = $"График зависимости шагов анализа от количества вершин с коэффициентов развилок {fork}";
            chartStepsVertex.yTitle = "Количество шагов";
            chartStepsVertex.xTitle = "Количество вершин";
            chartStepsVertex.xColumn = "A";
            chartStepsVertex.yColumn = "F";
            chartStepsVertex.xlWorkSheet2 = sheetStepsVertex;

            ChartDescription chartTimesVertex = new ChartDescription();
            chartTimesVertex.title = $"График зависимости времени анализа от количества вершин с коэффициентов развилок {fork}";
            chartTimesVertex.yTitle = "Время анализа, мс";
            chartTimesVertex.xTitle = "Количество вершин";
            chartTimesVertex.xColumn = "A";
            chartTimesVertex.yColumn = "E";
            chartTimesVertex.xlWorkSheet2 = sheetTimeVertex;

            ChartDescription chartStepsElems = new ChartDescription();
            chartStepsElems.title = $"График зависимости шагов анализа от количества элементов с коэффициентов развилок {fork}";
            chartStepsElems.yTitle = "Количество шагов";
            chartStepsElems.xTitle = "Количество элементов";
            chartStepsElems.xColumn = "C";
            chartStepsElems.yColumn = "F";
            chartStepsElems.xlWorkSheet2 = sheetStepsElems;

            ChartDescription chartTimesElems = new ChartDescription();
            chartTimesElems.title = $"График зависимости времени анализа от количества элементов с коэффициентов развилок {fork}";
            chartTimesElems.yTitle = "Время анализа, мс";
            chartTimesElems.xTitle = "Количество элементов";
            chartTimesElems.xColumn = "C";
            chartTimesElems.yColumn = "E";
            chartTimesElems.xlWorkSheet2 = sheetTimeElems;

            foreach (var chart in new ChartDescription[] { chartStepsVertex, chartTimesVertex, chartStepsElems, chartTimesElems })
            {
                chart.xlWorkSheet = xlWorkSheet;
                chart.items = items;
                chart.startIndex = startIndex;
                chart.number = number;
                CreateChartDiagramm(chart);
            }

        }

        void saveChart()
        {
            Excel.Application xlApp;
            Excel.Workbook xlWorkBook;
            Excel.Worksheet xlWorkSheet;
            object misValue = System.Reflection.Missing.Value;

            xlApp = new Excel.Application();
            xlWorkBook = xlApp.Workbooks.Add(misValue);
            xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
            xlWorkSheet.Name = "Данные";

            var sheetTimeVertex = (Excel.Worksheet)xlWorkBook.Worksheets.Add(misValue, xlWorkSheet);
            sheetTimeVertex.Name = "Время от вершин";
            var sheetStepsVertex = (Excel.Worksheet)xlWorkBook.Worksheets.Add(misValue, sheetTimeVertex);
            sheetStepsVertex.Name = "Шаги от вершин";
            var sheetTimeElems = (Excel.Worksheet)xlWorkBook.Worksheets.Add(misValue, sheetStepsVertex);
            sheetTimeElems.Name = "Время от элементов";
            var sheetStepsElems = (Excel.Worksheet)xlWorkBook.Worksheets.Add(misValue, sheetTimeElems);
            sheetStepsElems.Name = "Шаги от элементов";

            xlWorkSheet.Cells[1, 1] = "Узлов";
            xlWorkSheet.Cells[1, 2] = "Связей";
            xlWorkSheet.Cells[1, 3] = "Элементов";
            xlWorkSheet.Cells[1, 4] = "Соотношение связей в узлам";
            xlWorkSheet.Cells[1, 5] = "Время анализа, мс";
            xlWorkSheet.Cells[1, 6] = "Шагов";
            xlWorkSheet.Cells[1, 7] = "Связей от элементов развилок";
            xlWorkSheet.Cells[1, 8] = "Соотношение связей от элементов развилок к всего связям";

            //add data
            var index = 2;

            int forkNumber = 5;
            double maxRatio = this.data.Max(p => p.forkRatio());
            double deltaRatio = maxRatio / forkNumber;
            for(int forkIndex = 0; forkIndex < forkNumber; forkIndex++)
            {
                List<SaveItem> items = new List<SaveItem>();
                var minRatio = deltaRatio * forkIndex;
                var maxRation = deltaRatio * (forkIndex + 1);
                if (forkIndex == forkNumber -1)
                {
                    maxRation *= 2;
                }
                foreach (var item in data)
                {
                    if (item.forkRatio() >= minRatio && item.forkRatio() < maxRation)
                        items.Add(item);
                }
                items.Sort(delegate (SaveItem x, SaveItem y)
                {
                    return x.vertexes.CompareTo(y.vertexes);
                });
                double forkValue = deltaRatio * (forkIndex + 0.5);
                Console.WriteLine($"start with {index} + {items.Count} for {forkValue.ToString("0.00")} {minRatio.ToString("0.00")}-{maxRatio.ToString("0.00")}");
                DataToExcel(xlWorkSheet, items, index);

                CreateChartDiagrammAll(xlWorkSheet, items, forkValue.ToString("0.00"), index, forkIndex, sheetTimeVertex, sheetStepsVertex, sheetTimeElems, sheetStepsElems);

                Console.WriteLine("End");
                index += items.Count;
            }



            string fileXls = @"d:\work\ido\epc\stat\bin\Debug\epc\stat.xls";
            File.Delete(fileXls);
            xlWorkBook.SaveAs(fileXls, Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
            xlWorkBook.Close(true, misValue, misValue);
            xlApp.Quit();

            releaseObject(xlWorkSheet);
            releaseObject(xlWorkBook);
            releaseObject(xlApp);
        }

        private void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                Console.WriteLine("Exception Occured while releasing object " + ex.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }
    }
}
