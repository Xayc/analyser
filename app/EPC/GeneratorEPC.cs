﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using app.Diagramm;

namespace app.EPC
{
    public class GeneratorEPC
    {
        int vertexNumber;
        double fork;
        int id;
        int depthFork;
        Diagramm.Diagramm diagramm = new Diagramm.Diagramm();

        int nextId()
        {
            id++;
            return id;
        }

        Vertex node(VertexTypesEPC type)
        {
            Vertex v = new Vertex();
            v.Text = "";
            v.Id = nextId();

            var typeidef = new VertexTypeEPC();
            typeidef.VertexType = type;

            v.Type = typeidef;
            diagramm.Add(v);
            return v;
        }

        int random(int max)
        {
            Random rand = new Random();
            return rand.Next(max);
        }

        Vertex generateLogical()
        {
            VertexTypesEPC[] types = { VertexTypesEPC.OR, VertexTypesEPC.XOR, VertexTypesEPC.AND };
            VertexTypesEPC tp = types[random(types.Length)];
            //tp = VertexTypesEPC.AND;
            return node(tp);
        }

        Edge edge(Vertex start, Vertex end, EdgeDirectionalType direcional = EdgeDirectionalType.Directional)
        {
            Edge e = new Edge();
            e.Id = nextId();
            e.DirectionalType = direcional;
            diagramm.Add(e);
            e.BeginId = start.Id;
            e.EndId = end.Id;
            return e;
        }

        public GeneratorEPC(int vertexNumber, double fork)
        {
            this.vertexNumber = vertexNumber;
            this.fork = fork;
        }

        Vertex generatePattern(Vertex startEvent, int currentFork, int currentDepth)
        {
            if (currentFork == 0 || currentDepth == 0)
            {
                var middletFunction = node(VertexTypesEPC.Function);
                edge(startEvent, middletFunction);

                var eventEnd = node(VertexTypesEPC.Event);
                edge(middletFunction, eventEnd);

                return eventEnd;
            }
            else
            {
                var condition = this.generateLogical();
                var conditionEnd = node((condition.Type as VertexTypeEPC).VertexType);
                edge(startEvent, condition);

                for (var i = 0; i < currentFork; i++)
                {
                    var functionInCondition = node(VertexTypesEPC.Function);
                    edge(condition, functionInCondition);

                    var eventInCondition = node(VertexTypesEPC.Event);
                    edge(functionInCondition, eventInCondition);

                    var middlePattern = generatePattern(eventInCondition, currentFork, currentDepth - 1);
                    edge(middlePattern, conditionEnd);
                }
                var functionEnd = node(VertexTypesEPC.Function);
                edge(conditionEnd, functionEnd);

                var eventEnd = node(VertexTypesEPC.Event);
                edge(functionEnd, eventEnd);
                return eventEnd;
            }
            
        }

        public Diagramm.Diagramm Generate()
        {
            diagramm = new Diagramm.Diagramm();
            var action = node(VertexTypesEPC.Event);

            Random r = new Random();

            Vertex last = action;
            while (diagramm.Vertexes.Count < this.vertexNumber)
            {
                if (r.NextDouble() <= this.fork)
                {
                    last = generatePattern(last, 3, 2);
                }
                else
                {
                    var count = diagramm.Vertexes.Count;
                    while (diagramm.Vertexes.Count < count + 63)
                        last = generatePattern(last, 0, 0);

                }
            }

            //var actionFinal = node(VertexTypesEPC.Event);
            //edge(last, actionFinal);

            diagramm.Close();
            return diagramm;
        }
    }
}
