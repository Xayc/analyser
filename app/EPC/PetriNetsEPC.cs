﻿using app.Petri;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace app.EPC
{
    class PetriNetsConvertorEPC : PetriNetsConvertor
    {
        List<VertexTypesEPC> conditions = new List<VertexTypesEPC> { VertexTypesEPC.OR, VertexTypesEPC.XOR, VertexTypesEPC.AND, VertexTypesEPC.Event, VertexTypesEPC.Function };
        public override bool FilterVertex(Diagramm.Vertex v)
        {
            var vertexType = (v.Type as VertexTypeEPC).VertexType;
            return conditions.IndexOf(vertexType) != -1;
        }

        public override PetriInOut CreatePetriNets(Diagramm.Vertex v)
        {
            var vertexType = (v.Type as VertexTypeEPC).VertexType;
            PetriInOut elem = new PetriInOut();

            if (vertexType == VertexTypesEPC.Event || vertexType == VertexTypesEPC.Function)
            {
                ElementBlock(elem, v);
            }
            if (vertexType == VertexTypesEPC.AND)
            {
                ElementAnd(elem, v);
            }
            if (vertexType == VertexTypesEPC.OR)
            {
                ElementOr(elem, v);
            }
            if (vertexType == VertexTypesEPC.XOR)
            {
                ElementXor(elem, v);
            }
            return elem;
        }

    }
}
