﻿using app.Diagramm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace app.EPC
{
    public class AnalyzerEPC : Analyzer
    {
        public enum StateTypes { Begin, Event, RelEvent, Function, RelFunction, Return, End, Condition, RelCondition, LineEnd }
        StateTypes State = StateTypes.Return;
        bool Ended = false;

        int Counter = 0;

        public Vertex LastVertexShape;

        public class ReturnPoint
        {
            public ShapeGeneric Shape;
            public StateTypes State;
            public Vertex LastVertexShape;
        }

        public Stack<ReturnPoint> ReturnStack = new Stack<ReturnPoint>();
        public List<ReturnPoint> History = new List<ReturnPoint>();
        List<ShapeGeneric> FinishShape = new List<ShapeGeneric>();

        ShapeGeneric _currentShape = null;

        public int Steps()
        {
            return Counter;
        }


        public void RaiseError(ShapeGeneric shape, string message)
        {
            throw new DiarammException(shape == null ? -1 : shape.Id, message + $" шаг {Counter}");
        }

        public void Clear()
        {
            foreach (var s in Diagramm.Shapes())
            {
                s.Analyzed = false;
            }
            _currentShape = null;
        }

        public void FindStartedVertex()
        {
            var items = Diagramm.Vertexes.FindAll(v => (v.Type as VertexTypeEPC).VertexType == VertexTypesEPC.Event && v.Incoming().Count == 0);
            foreach (var t in items)
                ReturnStack.Push(new ReturnPoint()
                {
                    State = StateTypes.Begin,
                    Shape = t
                });
        }

        public void BeginCheck()
        {
            if (ReturnStack.Count == 0)
                RaiseError(null, "Нет начального символа");
            Dictionary<VertexTypesEPC, int> maxOutgoing = new Dictionary<VertexTypesEPC, int>()
            {
                {VertexTypesEPC.Event, 1},
                {VertexTypesEPC.Function, 1},
            };
            Dictionary<VertexTypesEPC, int> maxIncoming = new Dictionary<VertexTypesEPC, int>()
            {
                {VertexTypesEPC.Event, 1},
                {VertexTypesEPC.Function, 1},
            };

            foreach (var v in Diagramm.Vertexes)
            {
                int max_out = maxOutgoing.GetValueOrDefault((v.Type as VertexTypeEPC).VertexType, 0);
                int max_in = maxIncoming.GetValueOrDefault((v.Type as VertexTypeEPC).VertexType, 0);
                if (max_out > 0 && v.Outgoing().Count > max_out)
                    RaiseError(v, "Слишком много исходящих связей");
                if (max_in > 0 && v.Incoming().Count > max_in)
                    RaiseError(v, "Слишком много входящих связей");
            }
        }

        public override void Run()
        {
            Clear();
            FindStartedVertex();
            BeginCheck();
            StateMachine();
            FinishCheck();
        }

        public bool IsFinish()
        {
            if (State != StateTypes.End)
            {
                return false;
            }
            return true;
        }

        public void Start()
        {
            Clear();
            FindStartedVertex();
            BeginCheck();
        }

        public int minTimeVertex(Vertex v)
        {
            if (v.MineTime >= 0)
                return v.MineTime;
           
            Stack<Vertex> st = new Stack<Vertex>();
            st.Push(v);
            while (st.Count > 0)
            {
                var current = st.Pop();
                if (current.MineTime >= 0)
                    continue;

                Counter++;
                current.MineTime = int.MaxValue;
                var egdes = current.Incoming();
                if (egdes.Count == 0)
                {
                    current.MineTime = v.Timer;
                }
                else if (egdes.All(e => e.BeginVertex.MineTime >= 0))
                {
                    current.MineTime = egdes.Min(e => e.BeginVertex.MineTime) + current.Timer;
                }
                else
                {
                    current.MineTime = -1;
                    st.Push(current);
                    foreach (var e in egdes)
                    {
                        st.Push(e.BeginVertex);
                    }
                }
                int i = 0;

            }
            return v.MineTime;
            /*
            foreach (var e in v.Incoming())
            {
                Counter++;

                var incomeV = e.BeginVertex;
                if (incomeV.MineTime >= 0)
                    return v.MineTime;

                st.Push(e.BeginVertex);

                var incomeV = st.Pop();
                int t = minTimeVertex(e.BeginVertex);
                if (t < time)
                    time = t;
            }
            if (v.Incoming().Count == 0)
                time = 0;
            v.MineTime = time + v.Timer;
            return v.MineTime;
            */
        }

        public override int MinTime()
        {
            foreach (var v in Diagramm.Vertexes)
            {
                v.MineTime = -1;
            }

            foreach(var shape in FinishShape)
            {
                Vertex v = shape as Vertex;
                if (v == null) continue;
                minTimeVertex(v);
            }
            return FinishShape.Min(s => s.Vertex().MineTime);

            Stack<Vertex> st = new Stack<Vertex>();
            FindStartedVertex();
            foreach (var v in ReturnStack.Select(t => t.Shape.Vertex()))
            {
                st.Push(v);
                v.MineTime = v.Timer;
            }

            while (st.Count > 0)
            {
                var v = st.Pop();
                foreach (var e in v.Outgoing())
                {
                    int time = v.MineTime + e.EndVertex.Timer;
                    if (time < e.EndVertex.MineTime)
                    {
                        e.EndVertex.MineTime = time;
                        st.Push(e.EndVertex);
                    }
                }
            }
            return FinishShape.Min(s => s.Vertex().MineTime);
        }

        public void NextStep()
        {
            History.Add(new ReturnPoint
            {
                State = State,
                Shape = CurrentShape(),
                LastVertexShape = LastVertexShape
            });
            Next();
            Counter++;
        }

        public void StateMachine()
        {
            while (State != StateTypes.End)
            {
                NextStep();
            }
        }

        VertexTypesEPC VertexType(Vertex v)
        {
            return (v.Type as VertexTypeEPC).VertexType;
        }

        public void FinishCheck()
        {
            if (State != StateTypes.End)
            {
                RaiseError(CurrentShape(), "Незвершённая последовательность");
            }
            if (!Ended)
            {
                RaiseError(CurrentShape(), "Нет окнчания");
            }

            Vertex dead = Diagramm.Vertexes.Find(t => (t.Type as VertexTypeEPC).VertexType == VertexTypesEPC.AND && !t.Analyzed
                && t.Incoming().Exists(e => e.Analyzed));
            if (dead != null)
                RaiseError(dead, "Deadlock");

            foreach (var s in Diagramm.Shapes())
            {
                if (s.Analyzed)
                    continue;
                RaiseError(s, "Непроанализированный элемент");
            }
        }

        public ShapeGeneric CurrentShape()
        {
            return _currentShape;
        }

        public List<ShapeGeneric> Neighbors()
        {
            List<ShapeGeneric> list = new List<ShapeGeneric>();
            var t = CurrentShape();
            if (t == null)
                return list;
            return t.Neighbors();
        }

        public List<Vertex> NeighborsVertex()
        {
            return new List<Vertex>(Neighbors().FindAll(t => t.ShapeType == ShapeType.Vertex).Select(t => t.Vertex()));
        }

        public List<Edge> NeighborsEdge()
        {
            return new List<Edge>(Neighbors().FindAll(t => t.ShapeType == ShapeType.Edge).Select(t => t.Edge()));
        }

        public List<Edge> NeighborsType(EdgeDirectionalType type)
        {
            return NeighborsEdge().FindAll(t => t.DirectionalType == type && !t.Analyzed);
        }
        public List<Vertex> NeighborsType(VertexTypesEPC type)
        {
            return NeighborsVertex().FindAll(t => VertexType(t) == type && !t.Analyzed);
        }

        public List<Edge> DirectionalEdges()
        {
            return NeighborsType(EdgeDirectionalType.Directional).FindAll(t => t.BeginVertex == CurrentShape());
        }

        public Edge DirectionalEdge()
        {
            return DirectionalEdges().FirstOrDefault();
        }

        public bool ReturnExist()
        {
            return ReturnStack.Count > 0;
        }

        public void Next()
        {
            bool analyzed = true;
            ShapeGeneric nextElement = null;
            StateTypes? nextState = null;
            switch (State)
            {
                case StateTypes.Return:
                    if (ReturnStack.Count == 0)
                    {
                        nextState = StateTypes.End;
                        break;
                    }
                    //RaiseError(CurrentShape(), "Нет данных для возврата");
                    var point = ReturnStack.Pop();
                    nextElement = point.Shape;
                    nextState = point.State;
                    LastVertexShape = point.LastVertexShape;
                    break;
                case StateTypes.Begin:
                    nextElement = DirectionalEdge();
                    nextState = StateTypes.RelEvent;
                    LastVertexShape = CurrentShape() as Vertex;
                    break;
                case StateTypes.Event:
                    nextElement = DirectionalEdge();
                    nextState = StateTypes.RelEvent;
                    if (nextElement == null)
                    {
                        nextState = StateTypes.LineEnd;
                        FinishShape.Add(CurrentShape());
                    }
                    LastVertexShape = CurrentShape() as Vertex;
                    break;
                case StateTypes.RelEvent:
                    nextElement = NeighborsType(VertexTypesEPC.Function).FirstOrDefault();
                    if (nextElement != null)
                    {
                        nextState = StateTypes.Function;
                        break;
                    }
                    List<VertexTypesEPC> conditions1 = new List<VertexTypesEPC> { VertexTypesEPC.OR, VertexTypesEPC.XOR, VertexTypesEPC.AND };
                    nextElement = NeighborsVertex().Find(t => !t.Analyzed && conditions1.IndexOf(VertexType(t)) != -1);
                    if (nextElement != null)
                    {
                        nextState = StateTypes.Condition;
                        break;
                    }
                    nextElement = NeighborsVertex().Find(t => conditions1.IndexOf(VertexType(t)) != -1);
                    if (nextElement != null)
                    {
                        nextState = StateTypes.Return;
                        break;
                    }
                    break;
                case StateTypes.Function:
                    nextElement = DirectionalEdge();
                    nextState = StateTypes.RelFunction;
                    LastVertexShape = CurrentShape() as Vertex;
                    break;
                case StateTypes.RelFunction:
                    nextElement = NeighborsType(VertexTypesEPC.Event).FirstOrDefault();
                    if (nextElement != null)
                    {
                        nextState = StateTypes.Event;
                        break;
                    }

                    List<VertexTypesEPC> conditions = new List<VertexTypesEPC> { VertexTypesEPC.OR, VertexTypesEPC.XOR, VertexTypesEPC.AND };
                    nextElement = NeighborsVertex().Find(t => !t.Analyzed && conditions.IndexOf(VertexType(t)) != -1);
                    if (nextElement != null)
                    {
                        nextState = StateTypes.Condition;
                        break;
                    }
                    nextElement = NeighborsVertex().Find(t => conditions.IndexOf(VertexType(t)) != -1);
                    if (nextElement != null)
                    {
                        nextState = StateTypes.Return;
                        break;
                    }
                    break;

                case StateTypes.Condition:
                    if (VertexType(CurrentShape().Vertex()) == VertexTypesEPC.AND)
                    {
                        //deadlock
                        if (NeighborsType(EdgeDirectionalType.Directional).FindAll(t => t.EndVertex == CurrentShape() && !t.Analyzed).Count > 0)
                        {
                            analyzed = false;
                            nextState = StateTypes.Return;
                            break;
                        }
                    }

                    nextElement = DirectionalEdge();
                    if (LastVertexShape == null)
                        RaiseError(CurrentShape(), "Неизвестен тип предыдущей фигуры");

                    if (VertexType(LastVertexShape) == VertexTypesEPC.Function)
                        nextState = StateTypes.RelFunction;
                    else
                        nextState = StateTypes.RelEvent;
                    break;

                case StateTypes.LineEnd:
                    Ended = true;
                    if (ReturnExist())
                        nextState = StateTypes.Return;
                    else
                        nextState = StateTypes.End;
                    break;
            }

            if (CurrentShape() != null && analyzed)
            {
                CurrentShape().Analyzed = true;
            }


            if (nextElement == null && nextState != StateTypes.LineEnd && nextState != StateTypes.Return && nextState != StateTypes.End)
            {
                RaiseError(CurrentShape(), "Ожидалась след. фигура");
            }

            // Непроанлизированные связи
            var notAnalyzed = new List<Edge>(DirectionalEdges());
            notAnalyzed.AddRange(NeighborsType(EdgeDirectionalType.NoDirectional));
            notAnalyzed = notAnalyzed.FindAll(t => t != nextElement);
            if (notAnalyzed.Count > 0 && analyzed)
            {
                ReturnStack.Push(new ReturnPoint()
                {
                    State = State,
                    Shape = CurrentShape(),
                    LastVertexShape = LastVertexShape
                });
            }

            if (nextState == null)
                RaiseError(CurrentShape(), "необработанное состояние");
            State = nextState.Value;
            _currentShape = nextElement;

        }

    }
}
