﻿using app.Petri;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace app.BPMN
{
    class PetriNetsConvertorBPMN : PetriNetsConvertor
    {
        List<VertexTypesBPMN> modelTypes = new List<VertexTypesBPMN> { VertexTypesBPMN.OR,
            VertexTypesBPMN.XOR, VertexTypesBPMN.AND, VertexTypesBPMN.EndEvent, VertexTypesBPMN.IntermediateEvent,
            VertexTypesBPMN.StartEvent, VertexTypesBPMN.Task};
        List<VertexTypesBPMN> events = new List<VertexTypesBPMN> { VertexTypesBPMN.EndEvent, VertexTypesBPMN.IntermediateEvent, VertexTypesBPMN.StartEvent};
        public override bool FilterVertex(Diagramm.Vertex v)
        {
            var vertexType = (v.Type as VertexTypeBPMN).VertexType;
            return modelTypes.IndexOf(vertexType) != -1;
        }

        public override PetriInOut CreatePetriNets(Diagramm.Vertex v)
        {
            var vertexType = (v.Type as VertexTypeBPMN).VertexType;
            PetriInOut elem = new PetriInOut();

            if (vertexType == VertexTypesBPMN.Task || events.IndexOf(vertexType) != -1)
            {
                ElementBlock(elem, v);
            }
            else if (vertexType == VertexTypesBPMN.AND)
            {
                ElementAnd(elem, v);
            }
            else if (vertexType == VertexTypesBPMN.OR)
            {
                ElementOr(elem, v);
            }
            else if (vertexType == VertexTypesBPMN.XOR)
            {
                ElementXor(elem, v);
            }
            else
            {
                throw new Exception("pass element");
            }
            return elem;
        }

    }
}
