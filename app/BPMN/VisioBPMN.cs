﻿using app.Diagramm;
using app.Idef3;
using app.VisioBridge;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Visio = Microsoft.Office.Interop.Visio;

namespace app.BPMN
{
    public class VisioBPMN : VisioDiagramm
    {
        static Dictionary<string, string> map = new Dictionary<string, string>
        {
            {"Start Event", "StartEvent"},
            {"Intermediate Event", "IntermediateEvent"},
            {"End Event", "EndEvent"},
            {"Task", "Task"},
            {"Gateway Parallel", "AND"},
            {"Gateway Inclusive", "OR"},
            {"Gateway Exclusive", "XOR"}
        };

        public override Diagramm.Diagramm CreateDiagramm(List<Visio.Shape> Shapes)
        {
            var d = new Diagramm.Diagramm();
            foreach (Visio.Shape shape in Shapes)
            {
                string name = shape.NameU.Split('.')[0];

                var data = VisioHelper.GetAllCustomData(shape);
                
                if (name == "Gateway")
                {
                    string gatewayType = VisioHelper.GetValueOfCustomShapeData(shape, "GatewayType");
                    Console.WriteLine(gatewayType);
                    if (gatewayType == null)
                    {
                        throw new DiarammException(shape.ID, "Не найден тип условия");
                    }
                    name += " " + gatewayType;
                }

                bool association = data.GetValueOrDefault("ConnectingObjectType", null) == "Association";

                if (name == "Sequence Flow" || name == "Dynamic Connector" || association)
                {
                    Edge e = new Edge();
                    e.Id = shape.ID;
                    d.Add(e);

                    e.Type = new EdgeTypeBPMV(association ? EdgesTypesBPMN.Association : EdgesTypesBPMN.Connect);

                    Visio.Cell cBegin = shape.CellsSRC[(short)Visio.VisSectionIndices.visSectionObject, (short)Visio.VisRowIndices.visRowLine, (short)Visio.VisCellIndices.visLineBeginArrow];
                    Visio.Cell cEnd = shape.CellsSRC[(short)Visio.VisSectionIndices.visSectionObject, (short)Visio.VisRowIndices.visRowLine, (short)Visio.VisCellIndices.visLineEndArrow];

                    bool arrowBegin = cBegin.get_ResultStr(Visio.VisUnitCodes.visNoCast) != "0";
                    bool arrowEnd = cEnd.get_ResultStr(Visio.VisUnitCodes.visNoCast) != "0";

                    foreach (Visio.Connect c in shape.Connects)
                    {
                        if (c.FromCell.Name == "BeginX")
                        {
                            e.BeginId = c.ToSheet.ID;
                        }
                        if (c.FromCell.Name == "EndX")
                        {
                            e.EndId = c.ToSheet.ID;
                        }
                    }

                    e.DirectionalType = EdgeDirectionalType.Directional;
                    if (!arrowBegin && !arrowEnd)
                    {
                        e.DirectionalType = EdgeDirectionalType.NoDirectional;
                    }
                    if (arrowBegin && arrowEnd)
                    {
                        throw new DiarammException(shape.ID, "Двухсторонняя связь");
                    }

                    if (arrowBegin)
                    {
                        int t = e.BeginId;
                        e.BeginId = e.EndId;
                        e.EndId = t;
                    }

                    continue;
                }

                string originalName = map.GetValueOrDefault(name, null);
                if (originalName == null)
                {
                    throw new DiarammException(shape.ID, $"Неизвестый тип {originalName}");
                }

                Vertex v = new Vertex();
                v.Text = shape.Text;
                v.Id = shape.ID;
                v.Type = VertexTypeBPMN.CreateFromType(originalName);
                v.Data = data;
                d.Add(v);
            }

            //foreach (Visio.Shape shape in Shapes)
            //{
            //    string name = shape.NameU.Split('.')[0];

            //    if (name == "Intermediate Event")
            //    {
            //        int pinId = 0;
            //        foreach (Visio.Connect c in shape.Connects)
            //        {
            //            if (c.FromCell.Name == "PinX")
            //            {
            //                pinId = c.ToSheet.ID;
            //            }
            //        }
            //        if (pinId == 0)
            //        {
            //            throw new DiarammException(shape.ID, $"Висячий таймер");
            //        }
            //        int timer = 0;
            //        if (!int.TryParse(shape.Text, out timer))
            //        {
            //            throw new DiarammException(shape.ID, $"Невалидное значение {shape.Text}");
            //        }

            //        d.IdToVertex[pinId].Timer = timer;
            //    }
            //}

            d.Close();
            return d;
        }
    }
}
