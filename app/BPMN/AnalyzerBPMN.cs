﻿using app.Diagramm;
using app.Vocabulary;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace app.BPMN
{
    public class AnalyzerBPMN : Analyzer
    {
        public void RaiseError(ShapeGeneric shape, string message)
        {
            throw new DiarammException(shape == null ? -1 : shape.Id, message);
        }

        string DataValue(Vertex v)
        {
            var data = new Dictionary<string, string>(v.Data);
            data.Remove("Name");
            return JsonConvert.SerializeObject(data);
        }


        void synonym()
        {
            var names = new Dictionary<string, Vertex>();
            foreach (var vertex in Diagramm.Vertexes)
            {
                if (!names.ContainsKey(vertex.Text))
                    names.Add(vertex.Text, vertex);
            }
            var synonymsGropus = Synonyms.GroupBy(names.Keys);
            foreach (var group in synonymsGropus)
            {
                if (group.Count < 2)
                    continue;

                var vertexex = group.Select(name => names[name]);
                foreach (var elem1 in vertexex)
                {
                    foreach(var elem2 in vertexex)
                    {
                        if (DataValue(elem1) != DataValue(elem2))
                        {
                            RaiseError(null, "Синонимическая группа с разными параметрами: " + String.Join(", ", group));
                        }
                    }
                }
            }
        }

        void antonym()
        {
            var names = new Dictionary<string, Vertex>();
            foreach (var vertex in Diagramm.Vertexes)
            {
                if (!names.ContainsKey(vertex.Text))
                    names.Add(vertex.Text, vertex);
            }
            var antonymGroups = Antonym.GroupBy(names.Keys);
            if (antonymGroups.Count == 0)
                return;

            List<string> list_names = new List<string>();
            foreach (var group in antonymGroups)
            {
                var vertexex = group.Select(name => names[name]);
                foreach (var elem1 in vertexex)
                {
                    foreach (var elem2 in vertexex)
                    {
                        if (elem2 == elem1)
                            continue;
                        if (DataValue(elem1) == DataValue(elem2))
                        {
                            RaiseError(null, "Антонимическая группа с одинаковыми параметрами: " + String.Join(", ", group));
                        }
                    }
                }
            }
        }

        bool convertibilityExists(Vertex v1, Vertex v2)
        {
            foreach (var edge in v1.Edges)
            {
                if ((edge.Type as EdgeTypeBPMV).Type != EdgesTypesBPMN.Association)
                {
                    continue;
                }
                if (edge.Vertexes().Contains(v2))
                    return true;
            }
            return false;
        }

        void convertibility()
        {
            var names = new Dictionary<string, Vertex>();
            foreach (var vertex in Diagramm.Vertexes)
            {
                if (!names.ContainsKey(vertex.Text))
                    names.Add(vertex.Text, vertex);
            }
            var antonymGroups = Antonym.GroupBy(names.Keys);
            if (antonymGroups.Count == 0)
                return;

            List<string> list_names = new List<string>();
            foreach (var group in antonymGroups)
            {
                var vertexex = group.Select(name => names[name]);
                foreach (var elem1 in vertexex)
                {
                    foreach (var elem2 in vertexex)
                    {
                        if (elem2 == elem1)
                            continue;
                       if (!convertibilityExists(elem1, elem2))
                       {
                            RaiseError(null, "Отсутствует конверсивная связь: " + String.Join(", ", group));
                        } 
                    }
                }
            }
        }

        List<Diagramm.Diagramm> SplitDiagramm()
        {
            var vertexes = new Dictionary<Vertex, HashSet<Vertex>>();
            foreach(var v in this.Diagramm.Vertexes)
            {
                var lst = new HashSet<Vertex>();
                lst.Add(v);
                vertexes.Add(v, lst);
            }
            foreach(var e in this.Diagramm.Edges)
            {
                var baselst = vertexes[e.BeginVertex];
                foreach (var v in e.Vertexes())
                {
                    var lst = vertexes[v];
                    baselst.UnionWith(lst);
                    lst.ToList().ForEach(t => vertexes[t] = baselst);
                }
            }
            var diagrammsSet = new HashSet<HashSet<Vertex>>(vertexes.Values);

            var diagramms = new List<Diagramm.Diagramm>();
            foreach (var set in diagrammsSet)
            {
                var d = new Diagramm.Diagramm();
                d.Vertexes.AddRange(set);
                diagramms.Add(d);
            }

            return diagramms;
        }

        bool find(Vertex v, List<string> set)
        {
            var title = Regex.Replace(v.Text + "", @"\s", "");
            return set.Exists(s => Regex.Replace(s + "", @"\s", "") == title);
        }

        void Nested()
        {
            var diagrams = SplitDiagramm();
            foreach(var d1 in diagrams)
            {
                var filter = d1.Vertexes.FindAll(v => (v.Type as VertexTypeBPMN).VertexType == VertexTypesBPMN.StartEvent && v.Text != "");
                var startNames = new List<string>(filter.Select(v => v.Text));

                if (startNames.Count == 0) continue;

                foreach(var d2 in diagrams)
                {
                    if (d1 == d2) continue;
                    var nestedVertex = d2.Vertexes.Find(v => find(v, startNames));
                    if (nestedVertex == null) continue;
                    RaiseError(nestedVertex, "Отсутствует вложенная связь с диаграммой: " + String.Join(", ", startNames));
                }
            }
        }

        public override void Run()
        {
            //convertibility();
            //synonym();
            //antonym();
            Nested();
        }

        public override int MinTime()
        {
            return 0;
        }

    }
}
