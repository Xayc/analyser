﻿using app.Diagramm;
using app.Idef5;
using app.Petri;
using app.VisioBridge;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace app.BPMN
{
    public enum VertexTypesBPMN
    {
        StartEvent,
        IntermediateEvent,
        EndEvent,
        Task,
        AND,
        OR,
        XOR
    };

    public class VertexTypeBPMN : ShapeFormBase
    {
        public static ShapeFormBase CreateFromType(string name)
        {
            var typeidef = new VertexTypeBPMN();
            typeidef.VertexType = map.First(t => t.Value == name).Key;
            return typeidef;
        }

        static Dictionary<VertexTypesBPMN, string> map = new Dictionary<VertexTypesBPMN, string>
        {
            {VertexTypesBPMN.StartEvent, "StartEvent"},
            {VertexTypesBPMN.IntermediateEvent, "IntermediateEvent"},
            {VertexTypesBPMN.EndEvent, "EndEvent"},
            {VertexTypesBPMN.Task, "Task"},
            {VertexTypesBPMN.AND, "AND"},
            {VertexTypesBPMN.OR, "OR"},
            {VertexTypesBPMN.XOR, "XOR"},
        };

        public VertexTypesBPMN VertexType;
        public override ShapeFormBase GetForm(string name)
        {
            return CreateFromType(name);
        }

        public override string Name
        {
            get
            {
                return map[VertexType];
            }
        }
    }

    public enum EdgesTypesBPMN
    {
        Connect,
        Association
    };


    public class EdgeTypeBPMV : ShapeType<EdgesTypesBPMN>
    {
        public static Dictionary<EdgesTypesBPMN, string> Map = new Dictionary<EdgesTypesBPMN, string>
        {
            {EdgesTypesBPMN.Connect, "Connect"},
            {EdgesTypesBPMN.Association, "Association"}
        };

        public override Dictionary<EdgesTypesBPMN, string> GetMap()
        {
            return Map;
        }

        public EdgeTypeBPMV(EdgesTypesBPMN t) : base(t)
        {
        }

        public EdgeTypeBPMV(string name) : base(name)
        {

        }
    }

    public class DiagrammTypeBPMN : DiagrammType
    {
        public override VisioDiagramm Visio()
        {
            return new VisioBPMN();
        }

        public override PetriNetsConvertor PetriNetsConvertor()
        {
            return new PetriNetsConvertorBPMN();
        }

        public override Analyzer GetAnalyzer()
        {
            return new AnalyzerBPMN();
        }
    }
}
