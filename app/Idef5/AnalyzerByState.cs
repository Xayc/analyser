﻿using app.Diagramm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace app.Idef5
{
    public abstract class AnalyzerByState : Analyzer
    {
        public override int MinTime()
        {
            return 0;
        }

        protected bool Ended = false;
        protected int Counter = 0;
        protected ShapeGeneric _currentShape = null;
        public Vertex LastVertexShape;

        protected StatesAnalyzer State = StatesAnalyzer.Return;

        protected class ReturnPoint
        {
            public ShapeGeneric Shape;
            public StatesAnalyzer State;
            public Vertex LastVertexShape;
        }

        protected Stack<ReturnPoint> ReturnStack = new Stack<ReturnPoint>();
        protected List<ReturnPoint> History = new List<ReturnPoint>();
        protected List<ShapeGeneric> FinishShape = new List<ShapeGeneric>();

        public void RaiseError(ShapeGeneric shape, string message)
        {
            throw new DiarammException(shape == null ? -1 : shape.Id, message + $" шаг {Counter}", shape);
        }

        protected abstract List<Vertex> StartVertex();

        public void FindStartedVertex()
        {
            var items = StartVertex();
            foreach (var t in items)
                ReturnStack.Push(new ReturnPoint()
                {
                    State = StatesAnalyzer.Begin,
                    Shape = t
                });
        }

        public void Clear()
        {
            foreach (var s in Diagramm.Shapes())
            {
                s.Analyzed = false;
            }
        }

        public void FinishCheck()
        {
            if (State != StatesAnalyzer.End)
            {
                RaiseError(CurrentShape(), "Незвершённая последовательность");
            }
            if (!Ended)
            {
                RaiseError(CurrentShape(), "Нет окнчания");
            }

            foreach (var s in Diagramm.Shapes())
            {
                if (s.Analyzed)
                    continue;
                RaiseError(s, "Непроанализированный элемент");
            }
        }

        public bool ReturnExist()
        {
            return ReturnStack.Count > 0;
        }

        public override void Run()
        {
            Clear();
            FindStartedVertex();
            BeginCheck();
            StateMachine();
            FinishCheck();
        }

        public void StateMachine()
        {
            while (State != StatesAnalyzer.End)
            {
                History.Add(new ReturnPoint
                {
                    State = State,
                    Shape = CurrentShape(),
                    LastVertexShape = LastVertexShape
                });
                Next();
                Counter++;
            }
        }

        protected abstract void BeginCheck();

        public ShapeGeneric CurrentShape()
        {
            return _currentShape;
        }

        public List<ShapeGeneric> Neighbors()
        {
            List<ShapeGeneric> list = new List<ShapeGeneric>();
            var t = CurrentShape();
            if (t == null)
                return list;
            return t.Neighbors();
        }

        public List<Vertex> NeighborsVertex()
        {
            return new List<Vertex>(Neighbors().FindAll(t => t.ShapeType == ShapeType.Vertex).Select(t => t.Vertex()));
        }

        public List<Edge> NeighborsEdge()
        {
            return new List<Edge>(Neighbors().FindAll(t => t.ShapeType == ShapeType.Edge).Select(t => t.Edge()));
        }

        public List<Edge> NeighborsType(EdgeDirectionalType type)
        {
            return NeighborsEdge().FindAll(t => t.DirectionalType == type && !t.Analyzed);
        }


        public List<Edge> DirectionalEdges()
        {
            return NeighborsType(EdgeDirectionalType.Directional).FindAll(t => t.BeginVertex == CurrentShape());
        }

        public Edge DirectionalEdge()
        {
            return DirectionalEdges().FirstOrDefault();
        }

        protected void Next()
        {
            var nextStep = new NextStep();

            if (State == StatesAnalyzer.Return)
            {
                if (ReturnStack.Count == 0)
                {
                    nextStep.NextState = StatesAnalyzer.End;
                }
                else
                { 
                    var point = ReturnStack.Pop();
                    nextStep.NextElement = point.Shape;
                    nextStep.NextState = point.State;
                    LastVertexShape = point.LastVertexShape;
                }
            }
            if (State == StatesAnalyzer.LineEnd)
            {
                Ended = true;
                if (ReturnExist())
                    nextStep.NextState = StatesAnalyzer.Return;
                else
                    nextStep.NextState = StatesAnalyzer.End;
            }
            NextDetail(nextStep);

            if (CurrentShape() != null && nextStep.Analyzed)
            {
                CurrentShape().Analyzed = true;
            }


            if (nextStep.NextElement == null && nextStep.NextState != StatesAnalyzer.LineEnd && 
                nextStep.NextState != StatesAnalyzer.Return && nextStep.NextState != StatesAnalyzer.End)
            {
                RaiseError(CurrentShape(), "Ожидалась след. фигура");
            }

            // Непроанлизированные связи
            var notAnalyzed = new List<Edge>(DirectionalEdges());
            notAnalyzed.AddRange(NeighborsType(EdgeDirectionalType.NoDirectional));
            notAnalyzed = notAnalyzed.FindAll(t => t != nextStep.NextElement);
            if (notAnalyzed.Count > 0 && nextStep.Analyzed)
            {
                ReturnStack.Push(new ReturnPoint()
                {
                    State = State,
                    Shape = CurrentShape(),
                    LastVertexShape = LastVertexShape
                });
            }

            if (nextStep.NextState == null)
                RaiseError(CurrentShape(), "необработанное состояние");
            State = nextStep.NextState;
            _currentShape = nextStep.NextElement;

        }

        protected abstract void NextDetail(NextStep nextStep);
    }

    public class NextStep
    {
        public bool Analyzed = true;
        public ShapeGeneric NextElement = null;
        public StatesAnalyzer NextState = null;
    }


    public class StatesAnalyzer
    {
        public string Name;
        public StatesAnalyzer(string name)
        {
            this.Name = name;
        }

        public static StatesAnalyzer Begin = new StatesAnalyzer("Begin");
        public static StatesAnalyzer End = new StatesAnalyzer("End");
        public static StatesAnalyzer Return = new StatesAnalyzer("Return");
        public static StatesAnalyzer LineEnd = new StatesAnalyzer("LineEnd");
    }
}
