﻿using app.Diagramm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace app.Idef5
{
    public class AnalyzerIdef5Composition : AnalyzerByState
    {
        public class StatesTypes : StatesAnalyzer
        {
            public StatesTypes(string name) : base(name) { }

            public static StatesAnalyzer Kind = new StatesAnalyzer("Kind");
            public static StatesAnalyzer RelationProcess = new StatesAnalyzer("RelationProcess");
            public static StatesAnalyzer Relation = new StatesAnalyzer("Relation");
            public static StatesAnalyzer Process = new StatesAnalyzer("Process");
        }

        protected override List<Vertex> StartVertex()
        {
            return Diagramm.Vertexes.FindAll(v => (v.Type as VertexTypeIdef5).Type == VertexTypesIdef5.Kind && v.Incoming().Count == 0);
        }

        protected override void BeginCheck()
        {
            if (ReturnStack.Count == 0)
                RaiseError(null, "Нет начального символа");

            VertexTypesIdef5[] vertexTypes = { VertexTypesIdef5.AND, VertexTypesIdef5.NOT, VertexTypesIdef5.OR, VertexTypesIdef5.Kind, VertexTypesIdef5.Process };
            EdgesTypesIdef5[] edgesTypes = { EdgesTypesIdef5.Connect, EdgesTypesIdef5.FirstOrder, EdgesTypesIdef5.StrongTransitionArrow, EdgesTypesIdef5.WeakTransition };
            var nonVertexType = Diagramm.Vertexes.Find(v => !vertexTypes.Contains((v.Type as VertexTypeIdef5).Type));
            if (nonVertexType != null)
                RaiseError(nonVertexType, "Неизвестный символ");
            var nonEdgeType = Diagramm.Edges.Find(e => !edgesTypes.Contains((e.Type as EdgeTypeIdef5).Type));
            if (nonEdgeType != null)
                RaiseError(nonEdgeType, "Неизвестный символ");
            var vertexOugoingMany = Diagramm.Vertexes.Find(v => v.Outgoing().Count > 1);
            if (vertexOugoingMany != null)
                RaiseError(vertexOugoingMany, "Много выходов");
            var nonOutgoingVertex = Diagramm.Vertexes.Find(v => (v.Type as VertexTypeIdef5).Type != VertexTypesIdef5.Kind && v.Outgoing().Count == 0);
            if (nonOutgoingVertex != null)
                RaiseError(nonOutgoingVertex, "Нет выходов");
        }


        VertexTypesIdef5 VertexType(Vertex v)
        {
            return (v.Type as VertexTypeIdef5).Type;
        }

        EdgesTypesIdef5 EdgeType(Edge v)
        {
            return (v.Type as EdgeTypeIdef5).Type;
        }

        public List<Vertex> NeighborsType(VertexTypesIdef5 type)
        {
            return NeighborsVertex().FindAll(t => VertexType(t) == type && !t.Analyzed);
        }

        public void CheckOneEdge()
        {
            var vertex = CurrentShape() as Vertex;
            if (vertex == null)
                RaiseError(CurrentShape(), "Ожидалась вершина");
        }

        protected override void NextDetail(NextStep nextStep)
        {
            EdgesTypesIdef5[] directedEdgesTypes = { EdgesTypesIdef5.FirstOrder, EdgesTypesIdef5.StrongTransitionArrow, EdgesTypesIdef5.WeakTransition };

            if (State == StatesAnalyzer.Begin)
            {
                nextStep.NextElement = DirectionalEdge();
                nextStep.NextState = StatesTypes.Relation;
                LastVertexShape = CurrentShape() as Vertex;

                if (!directedEdgesTypes.Contains(EdgeType(nextStep.NextElement as Edge)))
                {
                    RaiseError(CurrentShape(), "Неккоретный тип связи");
                }
            }
            if (State == StatesTypes.Kind)
            {
                nextStep.NextElement = DirectionalEdge();
                LastVertexShape = CurrentShape() as Vertex;
                if (nextStep.NextElement == null)
                { 
                    nextStep.NextState = StatesTypes.LineEnd;
                }
                else
                {
                    if (!directedEdgesTypes.Contains(EdgeType(nextStep.NextElement as Edge)))
                    {
                        RaiseError(CurrentShape(), "Неккоретный тип связи");
                    }
                    nextStep.NextState = StatesTypes.Relation;
                }
                
            }
            if (State == StatesTypes.RelationProcess)
            {
                var begin = (CurrentShape() as Edge).BeginVertex;
                if (VertexType(begin) == VertexTypesIdef5.Process)
                {
                    nextStep.NextElement = begin;
                    nextStep.NextState = StatesTypes.Process;
                }
            }
            if (State == StatesTypes.Process)
            {
                nextStep.NextState = StatesAnalyzer.Return;
            }
            if (State == StatesTypes.Relation)
            {
                var goToVertex = true;
                var vertexType = EdgeType(CurrentShape() as Edge);
                if (vertexType == EdgesTypesIdef5.StrongTransitionArrow || vertexType == EdgesTypesIdef5.WeakTransition)
                {
                    var edge = CurrentShape() as Edge;
                    var edgeIncome = edge.Incoming().FirstOrDefault(e => !e.Analyzed && EdgeType(e) == EdgesTypesIdef5.Connect);

                    if (edgeIncome != null)
                    {
                        goToVertex = false;

                        nextStep.NextElement = edgeIncome;
                        nextStep.NextState = StatesTypes.RelationProcess;
                    }
                }

                if (goToVertex)
                {
                    nextStep.NextElement = (CurrentShape() as Edge).EndVertex;
                    nextStep.NextState = StatesTypes.Kind;
                    VertexTypesIdef5[] vertexTypes = { VertexTypesIdef5.AND, VertexTypesIdef5.NOT, VertexTypesIdef5.OR, VertexTypesIdef5.Kind };


                    if (!vertexTypes.Contains(VertexType(nextStep.NextElement as Vertex)))
                    {
                        RaiseError(CurrentShape(), "Неккоретный тип вершины");
                    }
                }
                else
                {
                    ReturnStack.Push(new ReturnPoint()
                    {
                        State = State,
                        Shape = CurrentShape(),
                        LastVertexShape = LastVertexShape
                    });
                }
            }
        }

    }
}
