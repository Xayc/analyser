﻿using app.Diagramm;
using app.Petri;
using app.VisioBridge;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace app.Idef5
{
    public enum VertexTypesIdef5
    {
        Process,
        Kind,
        Individual,
        AND,
        OR,
        NOT
    };

    public enum EdgesTypesIdef5
    {
        Connect,
        StrongTransitionArrow,
        SecondOrder,
        WeakTransition,
        FirstOrder
    };

    public abstract class ShapeType<T> : ShapeFormBase
    {
        public abstract Dictionary<T, string> GetMap();

        public ShapeType()
        {
        }

        public ShapeType(T t)
        {
            this.Type = t;
        }

        public ShapeType(string name)
        {
            this.Type = GetMap().First(t => t.Value == name).Key;
        }

        public override string Name
        {
            get
            {
                return GetMap()[Type];
            }
        }

        public T Type;

        public override ShapeFormBase GetForm(string name)
        {
            return Activator.CreateInstance(GetType(), name) as ShapeFormBase;
        }
    }

    public class VertexTypeIdef5 : ShapeType<VertexTypesIdef5>
    {
        static readonly Dictionary<VertexTypesIdef5, string> map = new Dictionary<VertexTypesIdef5, string>
        {
            {VertexTypesIdef5.Process, "Process"},
            {VertexTypesIdef5.Kind, "Kind"},
            {VertexTypesIdef5.Individual, "Individual"},
            {VertexTypesIdef5.AND, "AND"},
            {VertexTypesIdef5.OR, "OR"},
            {VertexTypesIdef5.NOT, "NOT"},
        };

        public override Dictionary<VertexTypesIdef5, string> GetMap()
        {
            return map;
        }

        public VertexTypeIdef5(VertexTypesIdef5 t) : base(t)
        {
        }

        public VertexTypeIdef5(string name) : base(name)
        {

        }
    }

    public class EdgeTypeIdef5 : ShapeType<EdgesTypesIdef5>
    {
        public static Dictionary<EdgesTypesIdef5, string> Map = new Dictionary<EdgesTypesIdef5, string>
        {
            {EdgesTypesIdef5.Connect, "Connect"},
            {EdgesTypesIdef5.StrongTransitionArrow, "StrongTransitionArrow"},
            {EdgesTypesIdef5.SecondOrder, "SecondOrder"},
            {EdgesTypesIdef5.WeakTransition, "WeakTransition"},
            {EdgesTypesIdef5.FirstOrder, "FirstOrder"}
        };

        public override Dictionary<EdgesTypesIdef5, string> GetMap()
        {
            return Map;
        }

        public EdgeTypeIdef5(EdgesTypesIdef5 t) : base(t)
        {
        }

        public EdgeTypeIdef5(string name) : base(name)
        {

        }
    }

    public class DiagrammTypeIdef5 : DiagrammType
    {
        public override VisioDiagramm Visio()
        {
            return new VisioIdef5();
        }

        public override PetriNetsConvertor PetriNetsConvertor()
        {
            return null;
        }

        public override Analyzer GetAnalyzer()
        {
            return new AnalyzerIdef5();
        }
    }
}
