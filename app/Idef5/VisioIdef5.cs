﻿using app.Diagramm;
using app.Idef3;
using app.VisioBridge;
using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Visio = Microsoft.Office.Interop.Visio;

namespace app.Idef5
{
    public class VisioIdef5 : VisioDiagramm
    {
        static Dictionary<string, string> map = new Dictionary<string, string>
        {
            {"Kind Symbol", "Kind"},
            {"Process", "Process"},
            {"Process1", "Individual"},
            {"And Junction", "AND"},
            {"Or Junction", "OR"},
            {"Not Junction", "NOT"},
            {"Connect", "Connect"},
            {"Strong Transition Arrow", "StrongTransitionArrow"},
            {"SecondOrder", "SecondOrder"},
            {"Weak Transition", "WeakTransition"},
            {"Two-place Relation Symbol", "FirstOrder"},
        };

        string ArrowBegin(Visio.Shape shape)
        {
            Visio.Cell cBegin = shape.CellsSRC[(short)Visio.VisSectionIndices.visSectionObject, (short)Visio.VisRowIndices.visRowLine, (short)Visio.VisCellIndices.visLineBeginArrow];
            return cBegin.get_ResultStr(Visio.VisUnitCodes.visNoCast);
        }

        string ArrowEnd(Visio.Shape shape)
        {
            Visio.Cell cBegin = shape.CellsSRC[(short)Visio.VisSectionIndices.visSectionObject, (short)Visio.VisRowIndices.visRowLine, (short)Visio.VisCellIndices.visLineEndArrow];
            return cBegin.get_ResultStr(Visio.VisUnitCodes.visNoCast);
        }

        void SetShapeIdForEdge(Edge e, Visio.Shape shape)
        {
            foreach (Visio.Connect c in shape.Connects)
            {
                if (c.FromCell.Name == "BeginX")
                {
                    e.BeginId = c.ToSheet.ID;
                }
                if (c.FromCell.Name == "EndX")
                {
                    e.EndId = c.ToSheet.ID;
                }
            }
        }

        class ShapeTemp
        {
            public int Id;
            public Visio.Shape Shape;
            public bool IsVertex;
            public string Name;
            public string Text;
            public List<ShapeTemp> Items = new List<ShapeTemp>();

            public string GetFullText()
            {
                return Text + Items.Aggregate("", (sum, item) => sum + item.GetFullText());
            }
        }

        void SubShape(IEnumerable shapes, ShapeTemp top)
        {
            foreach (Visio.Shape shape in shapes)
            {
                string name = shape.NameU.Split('.')[0];
                string originalName = map.GetValueOrDefault(name, null);
                var s = new ShapeTemp();
                s.Id = shape.ID;
                s.Shape = shape;
                s.Name = originalName;
                s.Text = shape.Text;
                top.Items.Add(s);

                SubShape(shape.Shapes, s);
            }
        }

        List<ShapeTemp> ProcessShapes(IEnumerable<Visio.Shape> shapes)
        {
            var edgesTypes = EdgeTypeIdef5.Map.Values;
            var list = new List<ShapeTemp>();
            foreach (Visio.Shape shape in shapes)
            {
                string name = shape.NameU.Split('.')[0];

                if (name == "Sheet")
                    continue;
                string originalName = map.GetValueOrDefault(name, null);
                if (originalName == null)
                {
                    throw new DiarammException(shape.ID, $"Неизвестый тип {name}");
                }
                var s = new ShapeTemp();
                s.Id = shape.ID;
                s.Shape = shape;
                s.IsVertex = !edgesTypes.Contains(originalName);
                s.Name = originalName;
                s.Text = shape.Text;
                SubShape(shape.Shapes, s);
                list.Add(s);
            }
            return list;
        }

        void MapperShapes(IEnumerable<Visio.Shape> shapes, Dictionary<int, int> mapShapes, Visio.Shape top = null)
        {
            foreach (Visio.Shape shape in shapes)
            {
                var topShape = top == null ? shape : top;
                mapShapes[shape.ID] = topShape.ID;
                foreach (Visio.Shape sub in shape.Shapes)
                {
                    var subList = new List<Visio.Shape>();
                    foreach (Visio.Shape sub2 in sub.Shapes)
                    {
                        subList.Add(sub2);
                    }
                    MapperShapes(subList, mapShapes, topShape);
                }
            }
        }

        void ChangeDirection(Edge e)
        {
            int t = e.BeginId;
            e.BeginId = e.EndId;
            e.EndId = t;
        }

        public override Diagramm.Diagramm CreateDiagramm(List<Visio.Shape> Shapes)
        {
            List<ShapeTemp> listShapes = ProcessShapes(Shapes);
            Dictionary<int, ShapeTemp> mapVertex = listShapes.ToDictionary(x => x.Id, x => x);
            Dictionary<int, int> mapShapes = new Dictionary<int, int>();
            MapperShapes(Shapes, mapShapes);
            
            var d = new Diagramm.Diagramm();
            foreach (ShapeTemp pseudoShape in listShapes)
            {
                var shape = pseudoShape.Shape;
                if (pseudoShape.IsVertex)
                {
                    Vertex v = new Vertex();
                    v.Id = pseudoShape.Id;
                    v.Type = new VertexTypeIdef5(pseudoShape.Name);
                    v.Text = pseudoShape.GetFullText();
                    d.Add(v);
                    continue;
                }

                Edge e = new Edge();
                e.Id = pseudoShape.Id;
                d.Add(e);
                SetShapeIdForEdge(e, shape);
                e.Type = new EdgeTypeIdef5(pseudoShape.Name);
                var edgeType = e.Type as EdgeTypeIdef5;

                try
                {
                    e.BeginId = mapShapes[e.BeginId];
                    e.EndId = mapShapes[e.EndId];
                }
                catch (KeyNotFoundException)
                {
                    d.Save("tmp.json");
                    throw new DiarammException(e.Id, "Неполня связь");
                }

                if (edgeType.Type == EdgesTypesIdef5.StrongTransitionArrow)
                {
                    e.DirectionalType = EdgeDirectionalType.Directional;
                    bool arrowBegin = ArrowBegin(shape) == "39";
                    if (arrowBegin)
                        ChangeDirection(e);
                    continue;
                }
                if (edgeType.Type == EdgesTypesIdef5.WeakTransition)
                {
                    e.DirectionalType = EdgeDirectionalType.Directional;
                    bool arrowBegin = ArrowBegin(shape) != "20";
                    if (arrowBegin)
                        ChangeDirection(e);
                    continue;
                }

                if (edgeType.Type == EdgesTypesIdef5.Connect)
                {
                    e.DirectionalType = EdgeDirectionalType.Directional;
                    bool arrowBegin = ArrowBegin(shape) != "0";
                    bool arrowEnd = ArrowBegin(shape) != "0";
                    if (!mapVertex[e.BeginId].IsVertex)
                        ChangeDirection(e);
                    continue;
                }
            }

            d.Close();
            return d;
        }
    }
}
