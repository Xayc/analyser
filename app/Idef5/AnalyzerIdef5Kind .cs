﻿using app.Diagramm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace app.Idef5
{
    public class AnalyzerIdef5Kind : AnalyzerByState
    {
        public class StatesTypes : StatesAnalyzer
        {
            public StatesTypes(string name) : base(name) { }

            public static StatesAnalyzer Kind = new StatesAnalyzer("Kind");
            public static StatesAnalyzer Relation = new StatesAnalyzer("Relation");
        }

        protected override List<Vertex> StartVertex()
        {
            return Diagramm.Vertexes.FindAll(v => (v.Type as VertexTypeIdef5).Type == VertexTypesIdef5.Kind && v.Incoming().Count == 0);
        }

        protected override void BeginCheck()
        {
            if (ReturnStack.Count == 0)
                RaiseError(null, "Нет начального символа");
            var nonVertexType = Diagramm.Vertexes.Find(v => (v.Type as VertexTypeIdef5).Type != VertexTypesIdef5.Kind);
            if (nonVertexType != null)
                RaiseError(nonVertexType, "Неизвестный символ");
            var nonEdgeType = Diagramm.Edges.Find(e => (e.Type as EdgeTypeIdef5).Type != EdgesTypesIdef5.SecondOrder);
            if (nonEdgeType != null)
                RaiseError(nonEdgeType, "Неизвестный символ");
            var vertexOugoingMany = Diagramm.Vertexes.Find(v => v.Outgoing().Count > 1);
            if (vertexOugoingMany != null)
                RaiseError(vertexOugoingMany, "Много выходов");
        }

        protected override void NextDetail(NextStep nextStep)
        {
            if (State == StatesAnalyzer.Begin)
            {
                nextStep.NextElement = DirectionalEdge();
                nextStep.NextState = StatesTypes.Relation;
                LastVertexShape = CurrentShape() as Vertex;
            }
            if (State == StatesTypes.Kind)
            {
                nextStep.NextElement = DirectionalEdge();
                LastVertexShape = CurrentShape() as Vertex;
                if (nextStep.NextElement == null)
                    nextStep.NextState = StatesTypes.LineEnd;
                else
                    nextStep.NextState = StatesTypes.Relation;
            }
            if (State == StatesTypes.Relation)
            {
                nextStep.NextElement = (CurrentShape() as Edge).EndVertex;
                nextStep.NextState = StatesTypes.Kind;
            }
        }

    }
}
