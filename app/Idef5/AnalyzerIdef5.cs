﻿using app.Diagramm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace app.Idef5
{
    public class AnalyzerIdef5 : Analyzer
    {
        public override void Run()
        {
            Analyzer a = null;
            var кindEdge = Diagramm.Edges.Find(e => (e.Type as EdgeTypeIdef5).Type == EdgesTypesIdef5.SecondOrder);
            var сomposionEdge = Diagramm.Edges.Find(e => (e.Type as EdgeTypeIdef5).Type == EdgesTypesIdef5.FirstOrder);

            if (кindEdge != null)
            {
                a = new AnalyzerIdef5Kind(); 
            }
            else
            {
                a = new AnalyzerIdef5Composition();
            }

            if (a == null)
            {
                throw new DiarammException(-1, "Неизвестный тип даиграммы");
            }
            a.Diagramm = Diagramm;
            a.Run();
        }

        public override int MinTime()
        {
            return 0;
        }

    }
}
