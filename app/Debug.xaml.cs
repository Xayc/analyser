﻿using app.Diagramm;
using app.EPC;
using app.VisioBridge;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Visio = Microsoft.Office.Interop.Visio;

namespace app
{

    public class HistoryItem
    {
        public int Id { get; set; }
        public string Type { get; set; }
        public string State { get; set; }
        public string Value { get; set; }
    }

    /// <summary>
    /// Логика взаимодействия для Debug.xaml
    /// </summary>
    public partial class Debug : Window
    {
        class NodeDescription
        {
            public string LineColor;
            public string LineWeight;
            public Visio.Shape Shape;
            public TypeSelectedNode CurrentStatus;
        };
        enum TypeSelectedNode { Analyzed, None, Current };
        Dictionary<int, NodeDescription> map = new Dictionary<int, NodeDescription>();
        AnalyzerEPC Analyzer;


        public Debug()
        {
            InitializeComponent();
            DrawButtons();
        }

        VisioPage GetVisioPage()
        {
            var vp = VisioPage.Create();
            vp.DiagrammType = new DiagrammTypeEPC();
            return vp;
        }

        void SelectNode(ShapeGeneric shape, TypeSelectedNode typeSelected)
        {
            var visioShape = map[shape.Id];
            var result = new NodeDescription();
            if (typeSelected == TypeSelectedNode.None)
            {
                result.LineColor = visioShape.LineColor;
                result.LineWeight = visioShape.LineWeight;
            }
            if (typeSelected == TypeSelectedNode.Current)
            {
                result.LineColor = "RGB(0,0,255)";
                result.LineWeight = "2 pt";
            }
            if (typeSelected == TypeSelectedNode.Analyzed)
            {
                result.LineColor = "RGB(0,255,0)";
                result.LineWeight = "2 pt";
            }
            VisioHelper.SetPropertyCell(visioShape.Shape, "LineColor", result.LineColor);
            VisioHelper.SetPropertyCell(visioShape.Shape, "LineWeight", result.LineWeight);
        }

        void RepaintNode()
        {
            var currentShape = Analyzer.CurrentShape();
            foreach (var node in Analyzer.Diagramm.Shapes())
            {
                var newStatus = TypeSelectedNode.None;
                if (currentShape == node)
                {
                    newStatus = TypeSelectedNode.Current;
                } else if (node.Analyzed)
                {
                    newStatus = TypeSelectedNode.Analyzed;
                }
                var visioShape = map[node.Id];
                if (visioShape.CurrentStatus != newStatus)
                {
                    visioShape.CurrentStatus = newStatus;
                    SelectNode(node, newStatus);
                }
            }
        }

        HistoryItem GetItemFromReturnPoint(AnalyzerEPC.ReturnPoint item)
        {
            string typeVertex = "";
            string value = "";
            if (item.Shape != null)
            {
                typeVertex = item.Shape.ShapeType == ShapeType.Edge ? "Дуга" : "Вершина";
                value = item.Shape.ShapeType == ShapeType.Edge ? "" : item.Shape.Vertex().Text;
            }
            return new HistoryItem { State = item.State.ToString(), Type = typeVertex, Value = value };
        }

        void DrawReturnStates()
        {
            var list = new List<HistoryItem>();
            int i = 1;
            if (Analyzer != null)
            {
                foreach (var item in Analyzer.ReturnStack)
                {
                    var t = GetItemFromReturnPoint(item);
                    t.Id = i;
                    list.Add(t);
                    i++;
                }
            }
            list.Reverse();
            ReturnGrid.ItemsSource = list;
        }

        void DrawStates()
        {
            var list = new List<HistoryItem>();
            int i = 1;
            if (Analyzer != null)
            {
                foreach (var item in Analyzer.History)
                {
                    var t = GetItemFromReturnPoint(item);
                    t.Id = i;
                    list.Add(t);
                    i++;
                }
            }
            list.Reverse();
            History.ItemsSource = list;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Analyzer.NextStep();
                if (Analyzer.IsFinish())
                {
                    Analyzer.FinishCheck();
                    Analyzer.Clear();
                    RepaintNode();
                    DrawButtons();
                    MessageBox.Show("Анализ закончен");
                    return;
                }
                RepaintNode();
                DrawButtons();
            }
            catch (DiarammException de)
            {
                MessageBox.Show(de.Message, "Ошибка");
            }
            
        }

        void DrawButtons()
        {
            DrawStates();
            DrawReturnStates();

            Start.IsEnabled = false;
            Next.IsEnabled = false;
            Stop.IsEnabled = false;
            if (Analyzer == null)
            {
                Start.IsEnabled = true;
                return;
            }
            if (Analyzer.IsFinish())
            {
                Start.IsEnabled = true;
                return;
            }
            Next.IsEnabled = true;
            Stop.IsEnabled = true;
        }

        private void Start_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                VisioPage vp = GetVisioPage();
                var d = vp.DiagrammType.Visio().CreateDiagramm(vp.Shapes);
                Analyzer = vp.DiagrammType.GetAnalyzer() as AnalyzerEPC;
                Analyzer.Diagramm = d;


                map.Clear();
                foreach (Visio.Shape shape in vp.Shapes)
                {
                    var description = new NodeDescription();
                    description.Shape = shape;
                    description.LineColor = VisioHelper.GetPropertyCell(shape, "LineColor");
                    description.LineWeight = VisioHelper.GetPropertyCell(shape, "LineWeight");
                    description.CurrentStatus = TypeSelectedNode.None;
                    map.Add(shape.ID, description);
                }
                Analyzer.Start();
            }
            catch (DiarammException de)
            {
                MessageBox.Show(de.Message, "Ошибка");
            }
            DrawButtons();
        }

        private void Stop_Click(object sender, RoutedEventArgs e)
        {
            if (Analyzer == null)
                return;
            Analyzer.Clear();
            RepaintNode();
            Analyzer = null;
            DrawButtons();
        }
    }
}
