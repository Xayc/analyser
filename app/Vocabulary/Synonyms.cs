﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YamlDotNet.RepresentationModel;

namespace app.Vocabulary
{
    public class SynonymsGroup: HashSet<string>
    {
    }

    public class SynonymsGroupList : List<SynonymsGroup>
    { }


    public static class Synonyms
    {
        static Dictionary<string, SynonymsGroup> groups = null;

        static Dictionary<string, SynonymsGroup> Groups()
        {
            if (groups == null)
                LoadGropus();
            return groups;
        }

        static void LoadGropus()
        {
            groups = new Dictionary<string, SynonymsGroup>();

            using (var reader = new StreamReader("Vocabulary/dict.yml"))
            {
                var yaml = new YamlStream();
                yaml.Load(reader);
                foreach(var doc in yaml.Documents)
                {
                    var sequence = (YamlSequenceNode)doc.RootNode;
                    foreach (var yamlGroup in sequence.Children)
                    {
                        var group = new SynonymsGroup();
                        var sequenceItem = (YamlSequenceNode)yamlGroup;
                        foreach (var item in sequenceItem.Children)
                        {
                            string value = ((YamlScalarNode)item).Value;
                            value = value.Trim().ToLower();
                            group.Add(value);
                            groups.Add(value, group);
                        }
                    }
                }
            }
        }

        public static SynonymsGroupList GroupBy(IEnumerable<string> items)
        {
            var local = new Dictionary<SynonymsGroup, SynonymsGroup>();
            foreach(string item in items)
            {
                var itemNormalize = item.Trim().ToLower();
                var group = Groups().GetValueOrDefault(itemNormalize, null);
                if (group == null)
                {
                    var newGroup = new SynonymsGroup();
                    newGroup.Add(item);
                    local.Add(newGroup, newGroup);
                    continue;
                }
                var localGroup = local.GetValueOrDefault(group, null);
                if (localGroup == null)
                {
                    localGroup = new SynonymsGroup();
                    local.Add(group, localGroup);
                }
                localGroup.Add(item);
            }

            var list = new SynonymsGroupList();
            list.AddRange(local.Values);
            return list;
        }
    }
}
