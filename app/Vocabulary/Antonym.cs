﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YamlDotNet.RepresentationModel;

namespace app.Vocabulary
{
    public class AntonymGroup : HashSet<string>
    {
    }

    public class AntonymGroupList : List<AntonymGroup>
    { }

    public static class Antonym
    {
        static Dictionary<string, AntonymGroup> groups = null;

        static Dictionary<string, AntonymGroup> Groups()
        {
            if (groups == null)
                LoadGropus();
            return groups;
        }

        static void LoadGropus()
        {
            groups = new Dictionary<string, AntonymGroup>();

            using (var reader = new StreamReader("Vocabulary/antonym.yml"))
            {
                var yaml = new YamlStream();
                yaml.Load(reader);
                foreach (var doc in yaml.Documents)
                {
                    var sequence = (YamlSequenceNode)doc.RootNode;
                    foreach (var yamlGroup in sequence.Children)
                    {
                        var group = new AntonymGroup();
                        var sequenceItem = (YamlSequenceNode)yamlGroup;
                        foreach (var item in sequenceItem.Children)
                        {
                            string value = ((YamlScalarNode)item).Value;
                            value = value.Trim().ToLower();
                            group.Add(value);
                            groups.Add(value, group);
                        }
                    }
                }
            }
        }

        public static AntonymGroupList GroupBy(IEnumerable<string> items)
        {
            var local = new Dictionary<AntonymGroup, AntonymGroup>();
            foreach (string item in items)
            {
                var itemNormalize = item.Trim().ToLower();
                var group = Groups().GetValueOrDefault(itemNormalize, null);
                if (group == null)
                {
                    continue;
                }
                var localGroup = local.GetValueOrDefault(group, null);
                if (localGroup == null)
                {
                    localGroup = new AntonymGroup();
                    local.Add(group, localGroup);
                }
                localGroup.Add(item);
            }

            var list = new AntonymGroupList();
            list.AddRange(local.Values);
            return list;
        }
    }
}
