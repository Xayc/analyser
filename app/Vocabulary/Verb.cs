﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YamlDotNet.RepresentationModel;

namespace app.Vocabulary
{
    public class Verb
    {
        static List<String> verbs = null;
        public static bool IsVerb(string text)
        {
            if (verbs == null)
                LoadVerbs();
            return verbs.Find(t => text.ToLower().Contains(t)) != null;
        }

        static void LoadVerbs()
        {
            verbs = new List<string>();

            using (var reader = new StreamReader("Vocabulary/verb.yml"))
            {
                var yaml = new YamlStream();
                yaml.Load(reader);
                foreach (var doc in yaml.Documents)
                {
                    var sequence = (YamlSequenceNode)doc.RootNode;
                    foreach (var yamlGroup in sequence.Children)
                    {
                        string value = ((YamlScalarNode)yamlGroup).Value;
                        value = value.Trim().ToLower();
                        verbs.Add(value);
                    }
                }
            }
        }
    }
}
