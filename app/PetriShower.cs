﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace app
{
    public static class PetriShower
    {
        public static void ShowNets(Petri.PetriNets nets)
        {
            var form = new System.Windows.Forms.Form();
            var viewer = new Microsoft.Msagl.GraphViewerGdi.GViewer();
            var graph = nets.MakeGraph();
            viewer.Graph = graph;
            form.SuspendLayout();
            viewer.Dock = System.Windows.Forms.DockStyle.Fill;
            form.Controls.Add(viewer);
            form.ResumeLayout();
            form.ShowDialog();
        }
    }
}
