﻿using app.Diagramm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace app.Idef3
{
    public class AnalyzerIdef3 : Analyzer
    {
        enum StateTypes { Begin, Unit, RelUnit, Return, End, Condition, RelCondition, LineEnd }
        StateTypes State = StateTypes.Return;
        bool Ended = false;

        int Counter = 0;

        public Vertex LastVertexShape;

        class ReturnPoint
        {
            public ShapeGeneric Shape;
            public StateTypes State;
            public Vertex LastVertexShape;
        }

        Stack<ReturnPoint> ReturnStack = new Stack<ReturnPoint>();
        List<ReturnPoint> History = new List<ReturnPoint>();
        List<ShapeGeneric> FinishShape = new List<ShapeGeneric>();

        ShapeGeneric _currentShape = null;

        List<VertexTypesIdef3> conditionsVertexType = new List<VertexTypesIdef3> { VertexTypesIdef3.OR, VertexTypesIdef3.XOR, VertexTypesIdef3.AND };


        public void RaiseError(ShapeGeneric shape, string message)
        {
            throw new DiarammException(shape == null ? -1 : shape.Id, message + $" шаг {Counter}");
        }

        public void Clear()
        {
            foreach (var s in Diagramm.Shapes())
            {
                s.Analyzed = false;
            }
        }

        public void FindStartedVertex()
        {
            var items = Diagramm.Vertexes.FindAll(v => VertexType(v) == VertexTypesIdef3.Unit && v.Incoming().Count == 0);
            if (items.Count > 1)
                RaiseError(null, "Начальных символов больше 1");

            foreach (var t in items)
                ReturnStack.Push(new ReturnPoint()
                {
                    State = StateTypes.Begin,
                    Shape = t
                });
        }

        public void BeginCheck()
        {
            if (ReturnStack.Count == 0)
                RaiseError(null, "Нет начального символа");
            Dictionary<VertexTypesIdef3, int> maxOutgoing = new Dictionary<VertexTypesIdef3, int>()
            {
                {VertexTypesIdef3.Unit, 1},
            };
            Dictionary<VertexTypesIdef3, int> maxIncoming = new Dictionary<VertexTypesIdef3, int>()
            {
                {VertexTypesIdef3.Unit, 1},
            };

            foreach (var v in Diagramm.Vertexes)
            {
                int max_out = maxOutgoing.GetValueOrDefault(VertexType(v), 0);
                int max_in = maxIncoming.GetValueOrDefault(VertexType(v), 0);
                if (max_out > 0 && v.Outgoing().Count > max_out)
                    RaiseError(v, "Слишком много исходящих связей");
                if (max_in > 0 && v.Incoming().Count > max_in)
                    RaiseError(v, "Слишком много входящих связей");
            }
        }

        public override void Run()
        {
            Clear();
            FindStartedVertex();
            BeginCheck();
            StateMachine();
            FinishCheck();
        }

        public override int MinTime()
        {
            var path = new Dictionary<Vertex, HashSet<Vertex>>();
            foreach (var v in Diagramm.Vertexes)
            {
                v.MineTime = int.MaxValue;
            }

            Stack<Vertex> st = new Stack<Vertex>();
            FindStartedVertex();
            foreach (var v in ReturnStack.Select(t => t.Shape.Vertex()))
            {
                st.Push(v);
                v.MineTime = v.Timer;
                var set = new HashSet<Vertex>();
                set.Add(v);
                path.Add(v, set);
            }

            while (st.Count > 0)
            {
                var v = st.Pop();
                foreach (var e in v.Outgoing())
                {
                    int time = 0;
                    var incmoing_vertex = new HashSet<Vertex>();
                    if (VertexType(e.EndVertex) == VertexTypesIdef3.AND)
                    {
                        foreach (var incoming_edge in e.EndVertex.Incoming())
                        {
                            if (!path.ContainsKey(incoming_edge.BeginVertex))
                            {
                                incmoing_vertex = null;
                                break;
                            }
                            foreach (var concat_vertes in path[incoming_edge.BeginVertex])
                                incmoing_vertex.Add(concat_vertes);
                        }
                        if (incmoing_vertex != null)
                            time = incmoing_vertex.Sum(iv => iv.Timer);
                    }
                    else
                    {
                        time = v.MineTime + e.EndVertex.Timer;
                        incmoing_vertex = new HashSet<Vertex>(path[v]);
                    }
                    if (incmoing_vertex != null && time < e.EndVertex.MineTime)
                    {
                        e.EndVertex.MineTime = time;
                        st.Push(e.EndVertex);

                        incmoing_vertex.Add(e.EndVertex);
                        path[e.EndVertex] = incmoing_vertex;
                    }
                }
            }
            return FinishShape.Min(s => s.Vertex().MineTime);
        }

        public void StateMachine()
        {
            while (State != StateTypes.End)
            {
                History.Add(new ReturnPoint
                {
                    State = State,
                    Shape = CurrentShape(),
                    LastVertexShape = LastVertexShape
                });
                Next();
                Counter++;
                Console.WriteLine(State);
            }
        }

        VertexTypesIdef3 VertexType(Vertex v)
        {
            return (v.Type as VertexTypeIdef3).VertexType;
        }

        public void FinishCheck()
        {
            if (State != StateTypes.End)
            {
                RaiseError(CurrentShape(), "Незвершённая последовательность");
            }
           

            Vertex dead = Diagramm.Vertexes.Find(t => VertexType(t) == VertexTypesIdef3.AND && !t.Analyzed
                && t.Incoming().Exists(e => e.Analyzed));
            if (dead != null)
                RaiseError(dead, "Deadlock");

            if (!Ended)
            {
                RaiseError(CurrentShape(), "Нет окончания");
            }

            foreach (var s in Diagramm.Shapes())
            {
                if (s.Analyzed)
                    continue;
                RaiseError(s, "Непроанализированный элемент");
            }
        }

        public ShapeGeneric CurrentShape()
        {
            return _currentShape;
        }

        public List<ShapeGeneric> Neighbors()
        {
            List<ShapeGeneric> list = new List<ShapeGeneric>();
            var t = CurrentShape();
            if (t == null)
                return list;
            return t.Neighbors();
        }

        public List<Vertex> NeighborsVertex()
        {
            return new List<Vertex>(Neighbors().FindAll(t => t.ShapeType == ShapeType.Vertex).Select(t => t.Vertex()));
        }

        public List<Edge> NeighborsEdge()
        {
            return new List<Edge>(Neighbors().FindAll(t => t.ShapeType == ShapeType.Edge).Select(t => t.Edge()));
        }

        public List<Edge> NeighborsType(EdgeDirectionalType type)
        {
            return NeighborsEdge().FindAll(t => t.DirectionalType == type && !t.Analyzed);
        }
        public List<Vertex> NeighborsType(VertexTypesIdef3 type)
        {
            return NeighborsVertex().FindAll(t => VertexType(t) == type && !t.Analyzed);
        }

        public List<Edge> DirectionalEdges()
        {
            return NeighborsType(EdgeDirectionalType.Directional).FindAll(t => t.BeginVertex == CurrentShape());
        }

        public Edge DirectionalEdge()
        {
            return DirectionalEdges().FirstOrDefault();
        }

        public bool ReturnExist()
        {
            return ReturnStack.Count > 0;
        }

        public void Next()
        {
            bool analyzed = true;
            ShapeGeneric nextElement = null;
            StateTypes? nextState = null;
            switch (State)
            {
                case StateTypes.Return:
                    if (ReturnStack.Count == 0)
                    {
                        nextState = StateTypes.End;
                        break;
                    }
                    //RaiseError(CurrentShape(), "Нет данных для возврата");
                    var point = ReturnStack.Pop();
                    nextElement = point.Shape;
                    nextState = point.State;
                    LastVertexShape = point.LastVertexShape;
                    break;
                case StateTypes.Begin:
                    nextElement = DirectionalEdge();
                    nextState = StateTypes.RelUnit;
                    LastVertexShape = CurrentShape() as Vertex;
                    break;
                case StateTypes.Unit:
                    nextElement = DirectionalEdge();
                    nextState = StateTypes.RelUnit;
                    if (nextElement == null)
                    {
                        nextState = StateTypes.LineEnd;
                        FinishShape.Add(CurrentShape());
                    }
                    LastVertexShape = CurrentShape() as Vertex;
                    break;
                case StateTypes.RelUnit:
                    nextElement = NeighborsType(VertexTypesIdef3.Unit).FirstOrDefault();
                    if (nextElement != null)
                    {
                        nextState = StateTypes.Unit;
                        break;
                    }
                    nextElement = NeighborsVertex().Find(t => !t.Analyzed && conditionsVertexType.IndexOf(VertexType(t)) != -1);
                    if (nextElement != null)
                    {
                        nextState = StateTypes.Condition;
                        break;
                    }
                    nextElement = NeighborsVertex().Find(t => conditionsVertexType.IndexOf(VertexType(t)) != -1);
                    if (nextElement != null)
                    {
                        nextState = StateTypes.Return;
                        break;
                    }
                    break;
                case StateTypes.Condition:
                    if (VertexType(CurrentShape().Vertex()) == VertexTypesIdef3.AND)
                    {
                        //deadlock
                        if (NeighborsType(EdgeDirectionalType.Directional).FindAll(t => t.EndVertex == CurrentShape() && !t.Analyzed).Count > 0)
                        {
                            analyzed = false;
                            nextState = StateTypes.Return;
                            break;
                        }
                    }

                    nextElement = DirectionalEdge();
                    if (LastVertexShape == null)
                        RaiseError(CurrentShape(), "Неизвестен тип предыдущей фигуры");

                    nextState = StateTypes.RelUnit;
                    break;

                case StateTypes.LineEnd:
                    Ended = true;
                    if (ReturnExist())
                        nextState = StateTypes.Return;
                    else
                        nextState = StateTypes.End;
                    break;
            }

            if (CurrentShape() != null && analyzed)
            {
                CurrentShape().Analyzed = true;
            }


            if (nextElement == null && nextState != StateTypes.LineEnd && nextState != StateTypes.Return && nextState != StateTypes.End)
            {
                RaiseError(CurrentShape(), "Ожидалась след. фигура");
            }

            // Непроанлизированные связи
            var notAnalyzed = new List<Edge>(DirectionalEdges());
            notAnalyzed.AddRange(NeighborsType(EdgeDirectionalType.NoDirectional));
            notAnalyzed = notAnalyzed.FindAll(t => t != nextElement);
            if (notAnalyzed.Count > 0 && analyzed)
            {
                ReturnStack.Push(new ReturnPoint()
                {
                    State = State,
                    Shape = CurrentShape(),
                    LastVertexShape = LastVertexShape
                });
            }

            if (nextState == null)
                RaiseError(CurrentShape(), "необработанное состояние");
            State = nextState.Value;
            _currentShape = nextElement;

        }
    }
}
