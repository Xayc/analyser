﻿using app.Diagramm;
using app.Petri;
using app.VisioBridge;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace app.Idef3
{
    public enum VertexTypesIdef3
    {
        Unit,
        AND,
        OR,
        XOR
    };

    public class VertexTypeIdef3 : ShapeFormBase
    {
        public static ShapeFormBase CreateFromType(string name)
        {
            var typeidef = new VertexTypeIdef3();
            typeidef.VertexType = map.First(t => t.Value == name).Key;
            return typeidef;
        }

        static Dictionary<VertexTypesIdef3, string> map = new Dictionary<VertexTypesIdef3, string>
        {
            {VertexTypesIdef3.Unit, "Unit"},
            {VertexTypesIdef3.AND, "AND"},
            {VertexTypesIdef3.OR, "OR"},
            {VertexTypesIdef3.XOR, "XOR"},
        };

        public VertexTypesIdef3 VertexType;
        public override ShapeFormBase GetForm(string name)
        {
            return VertexTypeIdef3.CreateFromType(name);
        }

        public override string Name
        {
            get
            {
                return map[VertexType];
            }
        }
    }

    public class DiagrammTypeIdef3 : DiagrammType
    {
        public override VisioDiagramm Visio()
        {
            return new VisioIdef3();
        }

        public override PetriNetsConvertor PetriNetsConvertor()
        {
            return new PetriNetsConvertorIdef3();
        }

        public override Analyzer GetAnalyzer()
        {
            return new AnalyzerIdef3();
        }
    }
}
