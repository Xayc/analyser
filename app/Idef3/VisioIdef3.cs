﻿using app.Diagramm;
using app.Idef3;
using app.VisioBridge;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Visio = Microsoft.Office.Interop.Visio;

namespace app.Idef3
{
    public class VisioIdef3 : VisioDiagramm
    {
        static Dictionary<string, string> map = new Dictionary<string, string>
        {
            {"Unit of Behaviour", "Unit"},
            {"AND", "AND"},
            {"OR", "OR"},
            {"XOR", "XOR"}
        };

        public override Diagramm.Diagramm CreateDiagramm(List<Visio.Shape> Shapes)
        {
            var d = new Diagramm.Diagramm();
            foreach (Visio.Shape shape in Shapes)
            {
                string name = shape.NameU.Split('.')[0];

                if (name == "Intermediate Event")
                    continue;

                if (name == "Sheet")
                    continue;

                if (name == "Simple Precedence" || name == "Dynamic connector")
                {
                    Diagramm.Edge e = new Diagramm.Edge();
                    e.Id = shape.ID;
                    d.Add(e);

                    Visio.Cell cBegin = shape.CellsSRC[(short)Visio.VisSectionIndices.visSectionObject, (short)Visio.VisRowIndices.visRowLine, (short)Visio.VisCellIndices.visLineBeginArrow];
                    Visio.Cell cEnd = shape.CellsSRC[(short)Visio.VisSectionIndices.visSectionObject, (short)Visio.VisRowIndices.visRowLine, (short)Visio.VisCellIndices.visLineEndArrow];

                    bool arrowBegin = cBegin.get_ResultStr(Visio.VisUnitCodes.visNoCast) != "0";
                    bool arrowEnd = cEnd.get_ResultStr(Visio.VisUnitCodes.visNoCast) != "0";

                    foreach (Visio.Connect c in shape.Connects)
                    {
                        if (c.FromCell.Name == "BeginX")
                        {
                            e.BeginId = c.ToSheet.ID;
                        }
                        if (c.FromCell.Name == "EndX")
                        {
                            e.EndId = c.ToSheet.ID;
                        }
                    }

                    e.DirectionalType = EdgeDirectionalType.Directional;
                    if (!arrowBegin && !arrowEnd)
                    {
                        e.DirectionalType = EdgeDirectionalType.NoDirectional;
                    }
                    if (arrowBegin && arrowEnd)
                    {
                        throw new DiarammException(shape.ID, "Двухсторонняя связь");
                    }

                    if (arrowBegin)
                    {
                        int t = e.BeginId;
                        e.BeginId = e.EndId;
                        e.EndId = t;
                    }

                    continue;
                }

                string originalName = map.GetValueOrDefault(name, null);
                if (originalName == null)
                {
                    throw new DiarammException(shape.ID, $"Неизвестый тип {originalName}");
                }

                //var a = shape.ConnectedShapes(Visio.VisConnectedShapesFlags.visConnectedShapesIncomingNodes, "");
                //var b = shape.ConnectedShapes(Visio.VisConnectedShapesFlags.visConnectedShapesOutgoingNodes, "");

                Vertex v = new Vertex();
                v.Id = shape.ID;
                v.Type = VertexTypeIdef3.CreateFromType(originalName);
                v.Text = shape.Text;
                d.Add(v);

                if ( (v.Type as VertexTypeIdef3).VertexType == VertexTypesIdef3.Unit)
                {
                    foreach (var sub in shape.Shapes)
                    {
                        v.Text += (sub as Visio.Shape).Text;
                        break;
                    }
                }

            }

            foreach (Visio.Shape shape in Shapes)
            {
                string name = shape.NameU.Split('.')[0];

                if (name == "Intermediate Event")
                {
                    int pinId = 0;
                    foreach (Visio.Connect c in shape.Connects)
                    {
                        if (c.FromCell.Name == "PinX")
                        {
                            pinId = c.ToSheet.ID;
                        }
                    }
                    if (pinId == 0)
                    {
                        throw new DiarammException(shape.ID, $"Висячий таймер");
                    }
                    int timer = 0;
                    if (!int.TryParse(shape.Text, out timer))
                    {
                        throw new DiarammException(shape.ID, $"Невалидное значение {shape.Text}");
                    }

                    if (!d.IdToVertex.ContainsKey(pinId))
                    {
                        throw new DiarammException(shape.ID, $"Висячий таймер без бокса");
                    }
                    d.IdToVertex[pinId].Timer = timer;
                }
            }

            d.Close();
            return d;
        }
    }
}
