﻿using app.Petri;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace app.Idef3
{
    class PetriNetsConvertorIdef3 : PetriNetsConvertor
    {
        List<VertexTypesIdef3> conditions = new List<VertexTypesIdef3> { VertexTypesIdef3.OR, VertexTypesIdef3.XOR, VertexTypesIdef3.AND, VertexTypesIdef3.Unit };
        public override bool FilterVertex(Diagramm.Vertex v)
        {
            var vertexType = (v.Type as VertexTypeIdef3).VertexType;
            return conditions.IndexOf(vertexType) != -1;
        }

        public override PetriInOut CreatePetriNets(Diagramm.Vertex v)
        {
            var vertexType = (v.Type as VertexTypeIdef3).VertexType;
            PetriInOut elem = new PetriInOut();

            if (vertexType == VertexTypesIdef3.Unit)
            {
                ElementBlock(elem, v);
            }
            if (vertexType == VertexTypesIdef3.AND)
            {
                ElementAnd(elem, v);
            }
            if (vertexType == VertexTypesIdef3.OR)
            {
                ElementOr(elem, v);
            }
            if (vertexType == VertexTypesIdef3.XOR)
            {
                ElementXor(elem, v);
            }
            return elem;
        }
    }
}
