﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace app.Petri
{
    /// <summary>
    ///  Дуги сети Петри
    /// </summary>
    public class Edge
    {
        public Node Node;
        public bool Inhibitor;
    }

    /// <summary>
    ///  Узлы сети Петри
    /// </summary>
    public class Node
    {
        public bool Place;
        public int Id;
        public int X;
        public int Y;

        // Исходящие дуги
        public List<Edge> OutgoingEdge = new List<Edge>();

        /// <summary>
        ///  Добавление исходящей дуги
        /// </summary>
        public void AddOutgoing(Node node, bool inhibitor = false)
        {
            Edge e = new Edge();
            e.Node = node;
            e.Inhibitor = inhibitor;
            OutgoingEdge.Add(e);
        }

        /// <summary>
        ///  Соседние узлы
        /// </summary>
        public IEnumerable<Node> Outgoing
        {
            get
            {
                return OutgoingEdge.Select(e => e.Node);
            }
        }

        /// <summary>
        ///  Устновка переключателя позция/переход
        /// </summary>
        public Node(bool place)
        {
            this.Place = place;
        }

        /// <summary>
        /// Идентификатор узла
        /// </summary>
        public string XmlId()
        {
            if (Place)
                return $"P{Id}";
            return $"T{Id}";
        }
    }

    /// <summary>
    ///  Сеть Петри
    /// </summary>
    public class PetriNets
    {
        public bool Horizontal = true;
        public List<Node> Nodes = new List<Node>();

        /// <summary>
        ///  Форирование XML узла
        /// </summary>
        public XElement LoadFromText(string s)
        {
            return XElement.Parse(s);
        }

        /// <summary>
        ///  Добавление узла в сеть Петри
        /// </summary>
        public Node AddNode(Node node)
        {
            Nodes.Add(node);
            return node;
        }

        /// <summary>
        ///  Добавление узла в сеть Петри
        /// </summary>
        public Node AddNode(bool place)
        {
            return AddNode(new Node(place));
        }

        /// <summary>
        ///  Закрятие сетри Петри для редактирования
        /// </summary>
        public void Close()
        {
            int n = 1;
            foreach (Node node in Nodes)
            {
                node.Id = n;
                n++;
            }
        }

        /// <summary>
        ///  Сохранение в файл
        /// </summary>
        public void Save(string file)
        {
            XDocument doc = new XDocument();

            XElement pnml = new XElement("pnml");
            doc.Add(pnml);

            XElement net = new XElement("net");
            pnml.Add(net);
            net.SetAttributeValue("id", "Net-One");
            net.SetAttributeValue("type", "P/T net");

            net.Add(LoadFromText("<token blue=\"0\" enabled=\"true\" green=\"0\" id=\"Default\" red=\"0\"/>"));

            int x = 100;
            foreach (Node node in Nodes)
            {
                var place = new XElement(node.Place ? "place" : "transition");
                place.SetAttributeValue("id", node.XmlId());
                net.Add(place);

                var graphics = new XElement("graphics");
                place.Add(graphics);

                var position = new XElement("position");
                position.SetAttributeValue("x", node.X);
                position.SetAttributeValue("y", node.Y);
                graphics.Add(position);

                var name = new XElement("name");
                place.Add(name);
                name.Add(new XElement("value", node.XmlId()));
                name.Add(new XElement("graphics",
                    new XElement("offset", new XAttribute[]{
                                    new XAttribute("x", "0.0"),
                                    new XAttribute("y", "0.0") }
                                )
                    ).ToXmlNode());

                if (node.Place)
                {
                    place.Add(LoadFromText("<initialMarking><value>Default,0</value><graphics><offset x=\"0.0\" y=\"0.0\"/></graphics></initialMarking>"));
                    place.Add(LoadFromText("<capacity><value>0</value></capacity>"));
                }
                else
                {
                    var angle = Horizontal ? "0" : "90";
                    place.Add(LoadFromText($"<orientation><value>{angle}</value></orientation>"));
                    place.Add(LoadFromText("<rate><value>1.0</value></rate>"));
                    place.Add(LoadFromText("<timed><value>false</value></timed>"));
                    place.Add(LoadFromText("<infiniteServer><value>false</value></infiniteServer>"));
                    place.Add(LoadFromText("<priority><value>1</value></priority>"));
                }

                x += 50;
            }

            foreach (Node node in Nodes)
            {
                foreach(var e in node.OutgoingEdge)
                {
                    var toNode = e.Node;
                    var arc = new XElement("arc");
                    arc.SetAttributeValue("id", $"{node.XmlId()} TO {toNode.XmlId()}");
                    arc.SetAttributeValue("source", node.XmlId());
                    arc.SetAttributeValue("target", toNode.XmlId());
                    net.Add(arc);

                    arc.Add(LoadFromText("<graphics/>"));
                    arc.Add(LoadFromText("<inscription><value>Default,1</value><graphics/></inscription>"));
                    arc.Add(LoadFromText("<tagged><value>false</value></tagged>"));

                    var typeArc = new XElement("type");
                    typeArc.SetAttributeValue("value", e.Inhibitor ? "inhibitor" : "normal");
                    arc.Add(typeArc);
                }
            }
            doc.Save(file);
        }

        /// <summary>
        ///  Расчёт положения узлов
        /// </summary>
        public void CalculateXY()
        {
            var graph = MakeGraph();
            var renderer = new Microsoft.Msagl.GraphViewerGdi.GraphRenderer(graph);
            renderer.CalculateLayout();

            foreach (var node in Nodes)
            {
                var gnode = graph.FindNode(node.Id.ToString());
                node.X = (int)gnode.BoundingBox.Center.Y;
                node.Y = (int)gnode.BoundingBox.Center.X;
            }
            int minX = Nodes.Min(t => t.X);
            int maxX = Nodes.Max(t => t.X);

            foreach (var node in Nodes)
            {
                node.X = (int)((maxX - node.X) * 1.4 + 50);
                node.Y = (int)(node.Y * 1.4) +  50;
            }

            if (!Horizontal)
            {
                foreach (var node in Nodes)
                {
                    var t = node.X;
                    node.X = node.Y;
                    node.Y = t;
                }
            }
        }

        /// <summary>
        ///  Постронение графа для сети Петри
        /// </summary>
        public Microsoft.Msagl.Drawing.Graph MakeGraph()
        {
            var graph = new Microsoft.Msagl.Drawing.Graph();

            foreach (var node in Nodes)
            {
                graph.AddNode(node.Id.ToString());

                foreach (var e in node.OutgoingEdge)
                {
                    var ge = graph.AddEdge(node.Id.ToString(), e.Node.Id.ToString());
                    if (e.Inhibitor)
                        ge.Attr.Color = Microsoft.Msagl.Drawing.Color.Red;
                }

                var gnode = graph.FindNode(node.Id.ToString());
                gnode.LabelText = "";
                if (node.Place)
                {
                    gnode.Attr.Shape = Microsoft.Msagl.Drawing.Shape.Circle;
                }
                else
                {
                    gnode.Attr.Shape = Microsoft.Msagl.Drawing.Shape.Box;
                    gnode.Attr.FillColor = Microsoft.Msagl.Drawing.Color.Black;
                }
            }
            return graph;
        }
    }
}
