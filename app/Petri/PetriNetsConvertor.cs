﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using app.Diagramm;

namespace app.Petri
{
    /// <summary>
    ///  Расширенная сеть Петри
    /// </summary>
    public class PetriInOut
    {
        public Petri.PetriNets Nets = new PetriNets();
        public Dictionary<Diagramm.Edge, Node> In = new Dictionary<Diagramm.Edge, Node>();
        public Dictionary<Diagramm.Edge, Node> Out = new Dictionary<Diagramm.Edge, Node>();
    }

    /// <summary>
    ///  Конвертер диграммы в Сеть Петри
    /// </summary>
    public abstract class PetriNetsConvertor
    {
        /// <summary>
        ///  Расчёт факториала
        /// </summary>
        public static int Factorial(int facno)
        {
            int temno = 1;

            for (int i = 1; i <= facno; i++)
            {
                temno = temno * i;
            }

            return temno;
        }

        /// <summary>
        ///  Расчёт перестановок
        /// </summary>
        public static int PermutaionCount(int n)
        {
            int permutationNumber = 0;
            for (var i = 0; i < n; i++)
            {
                int k = i + 1;
                permutationNumber += Factorial(n) / Factorial(n - k) / Factorial(k);
            }
            return permutationNumber;
        }

        /// <summary>
        ///  Фильтрация узлов для которых строится сеть Петри
        /// </summary>
        public abstract bool FilterVertex(Vertex v);

        /// <summary>
        ///  Формирование сети Петри для узла диграммы
        /// </summary>
        public abstract PetriInOut CreatePetriNets(Vertex v);

        /// <summary>
        ///  Трансляция диаграммы в сеть Петри
        /// </summary>
        public PetriNets PetriNets(Diagramm.Diagramm diagramm)
        {
            var nets = new Petri.PetriNets();
            nets.Horizontal = true;

            var map = new Dictionary<Diagramm.Vertex, PetriInOut>();

            foreach (Diagramm.Vertex v in diagramm.Vertexes)
            {
                if (!FilterVertex(v))
                    continue;

                PetriInOut elem = CreatePetriNets(v);
                map.Add(v, elem);
                nets.Nodes.AddRange(elem.Nets.Nodes);
            }

            foreach (var e in diagramm.Edges)
            {
                var start = map.GetValueOrDefault(e.BeginVertex, null);
                var end = map.GetValueOrDefault(e.EndVertex, null);

                if (start == null || end == null)
                    continue;

                start.Out[e].AddOutgoing(end.In[e]);
            }

            nets.Close();
            return nets;
        }

        /// <summary>
        ///  Трансляция последовательного узла диаграммы в сеть Петри
        /// </summary>
        public void ElementBlock(PetriInOut elem, Diagramm.Vertex v)
        {
            var place = elem.Nets.AddNode(true);
            var transition = elem.Nets.AddNode(false);
            place.AddOutgoing(transition);
            foreach (var source in v.Incoming())
            {
                elem.In.Add(source, place);
            }
            foreach (var dest in v.Outgoing())
            {
                elem.Out.Add(dest, transition);
            }
        }

        /// <summary>
        ///  Трансляция XOR-узла диаграммы в сеть Петри
        /// </summary>
        public void ElementXor(PetriInOut elem, Diagramm.Vertex v)
        {
            var outPlace = elem.Nets.AddNode(true);

            var incoming = v.Incoming();
            List<Petri.Node> inplaces = new List<Petri.Node>();
            foreach (var source in incoming)
            {
                var place = elem.Nets.AddNode(true);
                elem.In.Add(source, place);
                inplaces.Add(place);
            }
            for (var i = 0; i < incoming.Count; i++)
            {
                var transition = elem.Nets.AddNode(false);
                transition.AddOutgoing(outPlace);
                for (var k = 0; k < incoming.Count; k++)
                {
                    inplaces[k].AddOutgoing(transition, k != i);
                }
            }
            foreach (var dest in v.Outgoing())
            {
                var transition = elem.Nets.AddNode(false);
                elem.Out.Add(dest, transition);
                outPlace.AddOutgoing(transition);
            }
        }

        /// <summary>
        ///  Трансляция OR-узла диаграммы в сеть Петри
        /// </summary>
        public void ElementOr(PetriInOut elem, Diagramm.Vertex v)
        {
            var incoming = v.Incoming();
            int permutationNumber = PermutaionCount(incoming.Count);

            List<Petri.Node> inNodes = new List<Petri.Node>();
            foreach (var source in incoming)
            {
                var place = elem.Nets.AddNode(true);
                elem.In.Add(source, place);
                inNodes.Add(place);
            }

            var outPlace = elem.Nets.AddNode(true);

            for (var permutationState = 0; permutationState < permutationNumber; permutationState++)
            {
                int state = permutationState + 1;
                var transition = elem.Nets.AddNode(false);
                transition.AddOutgoing(outPlace);
                for (var i = 0; i < incoming.Count; i++)
                {
                    if (((state >> i) & 1) == 1)
                    {
                        inNodes[i].AddOutgoing(transition);
                    }
                    else
                    {
                        inNodes[i].AddOutgoing(transition, true);
                    }
                }
            }

            var outgoing = v.Outgoing();
            List<Petri.Node> outNodes = new List<Petri.Node>();
            foreach (var dest in outgoing)
            {
                var place = elem.Nets.AddNode(true);
                outNodes.Add(place);
                var transition = elem.Nets.AddNode(false);
                place.AddOutgoing(transition);
                elem.Out.Add(dest, transition);
            }

            int permutationNumberOut = PermutaionCount(outgoing.Count);
            for (var permutationState = 0; permutationState < permutationNumberOut; permutationState++)
            {
                int state = permutationState + 1;
                var transition = elem.Nets.AddNode(false);
                outPlace.AddOutgoing(transition);
                for (var i = 0; i < outgoing.Count; i++)
                {
                    if (((state >> i) & 1) == 1)
                    {
                        transition.AddOutgoing(outNodes[i]);
                    }
                }
            }
        }

        /// <summary>
        ///  Трансляция AND-узла диаграммы в сеть Петри
        /// </summary>
        public void ElementAnd(PetriInOut elem, Diagramm.Vertex v)
        {
            var transition = elem.Nets.AddNode(false);

            foreach (var source in v.Incoming())
            {
                var place = elem.Nets.AddNode(true);
                place.AddOutgoing(transition);
                elem.In.Add(source, place);
            }
            foreach (var dest in v.Outgoing())
            {
                elem.Out.Add(dest, transition);
            }
        }
    }
}
