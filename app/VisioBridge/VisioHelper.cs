﻿using System;
using System.Collections.Generic;
using Visio = Microsoft.Office.Interop.Visio;

namespace app.VisioBridge
{

    class VisioHelper
    {

        /// <summary>This method converts the input string to a Visio string by
        /// replacing each double quotation mark (") with a pair of double
        /// quotation marks ("") and then adding double quotation marks around
        /// the entire string.</summary>
        /// <param name="inputValue">Input string that will be converted
        /// to Visio String</param>
        /// <returns>Converted Visio string</returns>
        public static string StringToFormulaForString(string inputValue)
        {

            string result = "";
            string quote = "\"";
            string quoteQuote = "\"\"";

            try
            {

                result = inputValue != null ? inputValue : String.Empty;

                // Replace all (") with ("").
                result = result.Replace(quote, quoteQuote);

                // Add ("") around the whole string.
                result = quote + result + quote;
            }
            catch (Exception err)
            {
                System.Diagnostics.Debug.WriteLine(err.Message);
                throw;
            }

            return result;
        }

        public static Dictionary<string, string> GetAllCustomData(Visio.Shape shape)
        {
            short iRow = (short)Visio.VisRowIndices.visRowFirst;
            Dictionary<string, string> result = new Dictionary<string, string>();

            // While there are stil rows to look at.
            while (shape.get_CellsSRCExists((short)Visio.VisSectionIndices.visSectionProp, iRow,
                (short)Visio.VisCellIndices.visCustPropsValue, (short)0) != 0)
            {
                // Get the label and value of the current property.
                string label = shape.get_CellsSRC(
                                      (short)Visio.VisSectionIndices.visSectionProp,
                                      iRow,
                                      (short)Visio.VisCellIndices.visCustPropsLabel
                                      ).get_ResultStr(Visio.VisUnitCodes.visNoCast);

                string value = shape.get_CellsSRC(
                                       (short)Visio.VisSectionIndices.visSectionProp,
                                       iRow,
                                       (short)Visio.VisCellIndices.visCustPropsValue
                                       ).get_ResultStr(Visio.VisUnitCodes.visNoCast);

                result[label] = value;

                // Move to the next row in the properties
                iRow++;
            }
            return result;
        }

        public static string GetValueOfCustomShapeData(Visio.Shape shape, string LabelToSearch)
        {
            short iRow = (short)Visio.VisRowIndices.visRowFirst;

            // While there are stil rows to look at.
            while (shape.get_CellsSRCExists((short)Visio.VisSectionIndices.visSectionProp, iRow, 
                (short)Visio.VisCellIndices.visCustPropsValue, (short)0) != 0)
            {
                // Get the label and value of the current property.
                string label = shape.get_CellsSRC(
                                      (short)Visio.VisSectionIndices.visSectionProp,
                                      iRow,
                                      (short)Visio.VisCellIndices.visCustPropsLabel
                                      ).get_ResultStr(Visio.VisUnitCodes.visNoCast);

                string value = shape.get_CellsSRC(
                                       (short)Visio.VisSectionIndices.visSectionProp,
                                       iRow,
                                       (short)Visio.VisCellIndices.visCustPropsValue
                                       ).get_ResultStr(Visio.VisUnitCodes.visNoCast);

                if (label == LabelToSearch)
                    return value;

                // Move to the next row in the properties
                iRow++;
            }
            return null;
        }

        /// <summary>This method sets the value of the specified Visio cell
        /// to the new string passed as a parameter.</summary>
        /// <param name="formulaCell">Cell in which the value is to be set
        /// </param>
        /// <param name="newValue">New string value that will be set</param>
        public void SetCellValueToString(
        Visio.Cell formulaCell,
        string newValue)
        {

            try
            {

                // Set the value for the cell.
                formulaCell.FormulaU = StringToFormulaForString(newValue);
            }
            catch (Exception err)
            {
                System.Diagnostics.Debug.WriteLine(err.Message);
                throw;
            }
        }

        /// <summary>This method assigns a value to a custom property cell with
        /// the universal row name, specified via rowNameU parameter, in the
        /// shape, specified via shape parameter, using the value and units
        /// passed in as parameters.</summary>
        /// <param name="customPropertyShape">Shape which has the custom 
        /// property</param>
        /// <param name="rowNameU">Universal name of the custom property</param>
        /// <param name="propertyValue">Value of the custom property</param>
        /// <param name="units">Units of the value of the custom property
        /// </param>
        public void SetCustomPropertyValue(
            Microsoft.Office.Interop.Visio.Shape customPropertyShape,
            string rowNameU,
            object propertyValue,
            Microsoft.Office.Interop.Visio.VisUnitCodes units)
        {


            if (customPropertyShape == null || propertyValue == null ||
                rowNameU == null)
            {
                return;
            }

            const string CUST_PROP_PREFIX = "Prop.";

            Microsoft.Office.Interop.Visio.Application visioApplication =
                (Microsoft.Office.Interop.Visio.Application)
                customPropertyShape.Application;

            Microsoft.Office.Interop.Visio.Cell customPropertyCell;

            try
            {

                // Verify that all incoming string parameters are not of zero 
                // length, except for the ones that have default values as ""
                // and the output parameters.
                if (rowNameU.Length == 0)
                {

                    throw new System.ArgumentNullException("rowNameU",
                        "Zero length string input.");
                }

                // See if the shape has a custom property Value cell with the
                // universal row name. If no cell exists, display an error
                // message and exit this method.
                // Get the Cell object. Note the addition of "Prop." 
                // to the name given to the cell.
                customPropertyCell = customPropertyShape.get_CellsU(
                    CUST_PROP_PREFIX + rowNameU);

                if (units == Microsoft.Office.Interop.Visio.VisUnitCodes.
                    visUnitsString)
                {

                    SetCellValueToString(customPropertyCell,
                        propertyValue.ToString());

                }
                else
                {

                    // Use the set_Result method to set values other than 
                    // string type.
                    customPropertyCell.set_Result(units,
                        Convert.ToDouble(propertyValue,
                        System.Globalization.CultureInfo.InvariantCulture));
                }
            }
            catch (Exception err)
            {
                System.Diagnostics.Debug.WriteLine(err.Message);
                throw;
            }
        }

        public void connectFromShapeToConnector(Visio.Shape shapeFrom, Visio.Shape connector)
        {
            Microsoft.Office.Interop.Visio.Cell beginX;

            // Connect the begin point of the dynamic connector to the
            // PinX cell of the first 2-D shape.
            beginX = connector.get_CellsSRC(
                (short)Microsoft.Office.Interop.Visio.
                    VisSectionIndices.visSectionObject,
                (short)Microsoft.Office.Interop.Visio.
                    VisRowIndices.visRowXForm1D,
                (short)Microsoft.Office.Interop.Visio.
                    VisCellIndices.vis1DBeginX);

            beginX.GlueTo(shapeFrom.get_CellsSRC(
                (short)Microsoft.Office.Interop.Visio.
                    VisSectionIndices.visSectionObject,
                (short)Microsoft.Office.Interop.Visio.
                    VisRowIndices.visRowXFormOut,
                (short)Microsoft.Office.Interop.Visio.
                    VisCellIndices.visXFormPinX));
        }

        public void connectFromConnectorToShape(Visio.Shape connector, Visio.Shape shapeTo)
        {
            Microsoft.Office.Interop.Visio.Cell endX;
            endX = connector.get_CellsSRC(
                            (short)Microsoft.Office.Interop.Visio.
                                VisSectionIndices.visSectionObject,
                            (short)Microsoft.Office.Interop.Visio.
                                VisRowIndices.visRowXForm1D,
                            (short)Microsoft.Office.Interop.Visio.
                                VisCellIndices.vis1DEndX);

            endX.GlueTo(shapeTo.get_CellsSRC(
                (short)Microsoft.Office.Interop.Visio.
                    VisSectionIndices.visSectionObject,
                (short)Microsoft.Office.Interop.Visio.
                    VisRowIndices.visRowXFormOut,
                (short)Microsoft.Office.Interop.Visio.
                    VisCellIndices.visXFormPinX));
        }

        public static void SetPropertyCell(Visio.Shape shape, string field, String formula)
        {
            try
            {
                shape.Cells[field].FormulaU = formula;
            }
            catch
            {
            }
            foreach (Visio.Shape subShape in shape.Shapes)
            {
                try
                {
                    subShape.Cells[field].FormulaForceU = formula;
                }
                catch
                {
                }
            }
        }

        public static string GetPropertyCell(Visio.Shape shape, string field)
        {
            return shape.Cells[field].FormulaU;
        }

        public static void changeColor(Visio.Shape shape, String formula)
        {
            string result = shape.Cells["LineColor"].FormulaU;
            try
            {
                shape.Cells["LineColor"].FormulaU = formula;
            }
            catch
            {
            }
            foreach (Visio.Shape subShape in shape.Shapes)
            {
                try
                {
                    subShape.Cells["LineColor"].FormulaForceU = formula;
                }
                catch
                {
                }
            }
        }

    }
}
