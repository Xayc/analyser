﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Visio = Microsoft.Office.Interop.Visio;

namespace app.VisioBridge
{
    public abstract class VisioDiagramm
    {
        public abstract Diagramm.Diagramm CreateDiagramm(List<Visio.Shape> Shapes);
    }
}
