﻿using app.Idef3;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using app.Diagramm;
using Visio = Microsoft.Office.Interop.Visio;
using System.Runtime.InteropServices;
using app.Universal;

namespace app.VisioBridge
{
    public class VisioPage
    {
        public static VisioPage Create()
        {
            var app = (Visio.Application)Marshal.GetActiveObject("Visio.Application");
            Visio.Page targetPage = app.ActivePage;
            var vp = new VisioPage(targetPage);
            vp.SetSelection(app.ActiveWindow.Selection);
            return vp;
        }

        public List<Visio.Shape> Shapes = new List<Visio.Shape>();
        public DiagrammType DiagrammType;

        public void FillShapes(List<Visio.Shape> shapes)
        {
            this.Shapes = shapes;
            if(this.Shapes.Count == 0)
            {
                foreach (Visio.Shape shape in page.Shapes)
                {
                    this.Shapes.Add(shape);
                }
            }
        }

        public void SetSelection(Visio.Selection selection)
        {
            List<Visio.Shape> shapes = new List<Visio.Shape>();
            foreach (var c in selection)
            {
                Visio.Shape shape = c as Visio.Shape;
                if (shape != null)
                    shapes.Add(shape);
            }
            FillShapes(shapes);
        }

        static Dictionary<string, string> map = new Dictionary<string, string>
        {
            {"Event", "Event"},
            {"Function", "Function"},
            {"Process path", "Process"},
            {"Документ", "Information"},
            {"Organizational unit", "Unit"},
            {"AND", "AND"},
            {"OR", "OR"},
            {"XOR", "XOR"}
        };

        Visio.Page page;

        public VisioPage(Visio.Page page)
        {
            this.page = page;
        }

        public Diagramm.Diagramm CreateDiagramm()
        {
            return null;
            //Diagramm d = new Diagramm();
            //foreach (Visio.Shape shape in Shapes)
            //{
            //    string name = shape.NameU.Split('.')[0];

            //    if (name == "Intermediate Event")
            //        continue;

            //    if (name == "Dynamic connector")
            //    {
            //        Edge e = new Edge();
            //        e.Id = shape.ID;
            //        d.Add(e);

            //        Visio.Cell cBegin = shape.CellsSRC[(short)Visio.VisSectionIndices.visSectionObject, (short)Visio.VisRowIndices.visRowLine, (short)Visio.VisCellIndices.visLineBeginArrow];
            //        Visio.Cell cEnd = shape.CellsSRC[(short)Visio.VisSectionIndices.visSectionObject, (short)Visio.VisRowIndices.visRowLine, (short)Visio.VisCellIndices.visLineEndArrow];

            //        bool arrowBegin = cBegin.get_ResultStr(Visio.VisUnitCodes.visNoCast) != "0";
            //        bool arrowEnd = cEnd.get_ResultStr(Visio.VisUnitCodes.visNoCast) != "0";

            //        foreach (Visio.Connect c in shape.Connects)
            //        {
            //            if (c.FromCell.Name == "BeginX")
            //            {
            //                e.BeginId = c.ToSheet.ID;
            //            }
            //            if (c.FromCell.Name == "EndX")
            //            {
            //                e.EndId = c.ToSheet.ID;
            //            }
            //        }

            //        e.Type = EdgeType.Directional;
            //        if (!arrowBegin && !arrowEnd)
            //        {
            //            e.Type = EdgeType.NoDirectional;
            //        }
            //        if (arrowBegin && arrowEnd)
            //        {
            //            throw new DiarammException(shape.ID, "Двухсторонняя связь");
            //        }

            //        if (arrowBegin)
            //        {
            //            int t = e.BeginId;
            //            e.BeginId = e.EndId;
            //            e.EndId = t;
            //        }

            //        continue;
            //    }

            //    string originalName = map.GetValueOrDefault(name, null);
            //    if (originalName == null)
            //    {
            //        throw new DiarammException(shape.ID, $"Неизвестый тип {originalName}");
            //    }

            //    var a = shape.ConnectedShapes(Visio.VisConnectedShapesFlags.visConnectedShapesIncomingNodes, "");
            //    var b = shape.ConnectedShapes(Visio.VisConnectedShapesFlags.visConnectedShapesOutgoingNodes, "");

            //    Vertex v = new Vertex();
            //    v.Text = shape.Text;
            //    v.Id = shape.ID;
            //    v.Type = TypeMapper.GetVertexType(originalName);
            //    d.Add(v);

               
            //}

            //foreach (Visio.Shape shape in Shapes)
            //{
            //    string name = shape.NameU.Split('.')[0];

            //    if (name == "Intermediate Event")
            //    {
            //        int pinId = 0;
            //        foreach (Visio.Connect c in shape.Connects)
            //        {
            //            if (c.FromCell.Name == "PinX")
            //            {
            //                pinId = c.ToSheet.ID;
            //            }
            //        }
            //        if (pinId == 0)
            //        {
            //            throw new DiarammException(shape.ID, $"Висячий таймер");
            //        }
            //        int timer = 0;
            //        if (!int.TryParse(shape.Text, out timer))
            //        {
            //            throw new DiarammException(shape.ID, $"Невалидное значение {shape.Text}");
            //        }

            //        d.IdToVertex[pinId].Timer = timer;
            //    }
            //}

            //d.Close();
            //return d;
        }

        public Petri.PetriNets CreatePetri()
        {
            Dictionary<int, Visio.Shape> map = new Dictionary<int, Visio.Shape>();
            foreach (Visio.Shape shape in Shapes)
            {
                map.Add(shape.ID, shape);
            }
            try
            {
                var d = DiagrammType.Visio().CreateDiagramm(Shapes);
                d.Save($"{page.Name}.json");
                var nets = DiagrammType.PetriNetsConvertor().PetriNets(d);
                nets.CalculateXY();
                nets.Save($"{page.Name}-petri.xml");

                return nets;
            }
            catch (DiarammException e)
            {
                if (e.ShapeId != -1)
                {
                    var shape = map[e.ShapeId];
                    VisioHelper.changeColor(shape, "RGB(255,0,0)");
                    var text = shape.Text;
                    if ((e.Shape as Vertex) != null)
                        text = (e.Shape as Vertex).Text;
                    MessageBox.Show(e.Message, $"Ошибка {shape.Text} :{e.ShapeId}");
                }
                else
                {
                    MessageBox.Show(e.Message, "Ошибка");
                }

            }
            return null;
        }

        public TimeLineDiagramm TimeLineDiagramm()
        {
            Dictionary<int, Visio.Shape> map = new Dictionary<int, Visio.Shape>();
            foreach (Visio.Shape shape in Shapes)
            {
                map.Add(shape.ID, shape);
            }
            try
            {
                var d = DiagrammType.Visio().CreateDiagramm(Shapes);
                d.Save($"{page.Name}.json");


                TimeLineUniversal a = new TimeLineUniversal(d);
                return a.CreateTimeLine();

            }
            catch (DiarammException e)
            {
                if (e.ShapeId != -1)
                {
                    var shape = map[e.ShapeId];
                    VisioHelper.changeColor(shape, "RGB(255,0,0)");
                    var text = shape.Text;
                    if ((e.Shape as Vertex) != null)
                        text = (e.Shape as Vertex).Text;
                    MessageBox.Show(e.Message, $"Ошибка {text} :{e.ShapeId}");
                }
                else
                {
                    MessageBox.Show(e.Message, "Ошибка");
                }
            }
            return null;
        }

        public void Analyze()
        {
            Dictionary<int, Visio.Shape> map = new Dictionary<int, Visio.Shape>();
            foreach (Visio.Shape shape in Shapes)
            {
                map.Add(shape.ID, shape);
            }
            try
            {
                var d = DiagrammType.Visio().CreateDiagramm(Shapes);
                d.Save($"{page.Name}.json");

                Analyzer a = DiagrammType.GetAnalyzer();
                if (a == null)
                {
                    return;
                }
                a.Diagramm = d;
                a.Run();

                //MessageBox.Show($"Минимальное время {a.MinTime()}", "Закончено");
            }
            catch (DiarammException e)
            {
                if (e.ShapeId != -1)
                {
                    var shape = map[e.ShapeId];
                    VisioHelper.changeColor(shape, "RGB(255,0,0)");
                    var text = shape.Text;
                    if ((e.Shape as Vertex) != null)
                        text = (e.Shape as Vertex).Text;
                    MessageBox.Show(e.Message, $"Ошибка {text} :{e.ShapeId}");
                }
                else
                {
                    MessageBox.Show(e.Message, "Ошибка");
                }

            }
        }
    }
}
